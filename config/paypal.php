<?php
return array(
    // set your paypal credential
    'client_id' => 'AfJZ9Q_7GXvU-wWeo8w5YVHsmhRmwQ09kyPDzqDMvWyWPw6eQoHaTtXl0BzrT967WDXBtCT4g1ClF3Hj',
    'secret' => 'EHnnvHqV5uzbt8mtsOVWvHDcTOIGXuFm5y3_vmxxdKVcohH7I38E03je07INr1rE6y3esQlelB1oFSHr',
    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);