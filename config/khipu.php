<?php
return array(

	/*
	|--------------------------------------------------------------------------
	| ID
	|--------------------------------------------------------------------------
	|
	| This id is your 'Id de Cobrador'
	| you can find out how to get it here:
	| "https://khipu.com/merchant/profile"
	|
	*/
	// id de prueba
	//  'id' => env('KHIPU_ID', '75007'),
		'id' => env('KHIPU_ID', '79564'),
    /*
	|--------------------------------------------------------------------------
	| KEY
	|--------------------------------------------------------------------------
	|
	| This id is your 'Llave'
	| you can find out how to get it here:
	| "https://khipu.com/merchant/profile"
	|
	*/
	// key prueba fernando
//	'key' => env('KHIPU_KEY', '868bb39358c6e442cb5389be4262990f0a4e2def'),
	'key' => env('KHIPU_KEY', 'ba453ea27b0963f85445b4dc4424c41c0d245e90'),
);