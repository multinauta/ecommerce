
<?php

if($control==0 or $control==null)
{
$seleccion1='selected';
$seleccion2='';
}
else
{
$seleccion2='selected';
$seleccion1='';
}
?>


	@extends('admin.template')

	@section('content')
	<div class="container text-center">
	
	<div class="page-header">
		<ul class="nav nav-pills">
		 <li role="presentation" class="active"><a href="{{ route('admin.stock.index') }}">Stock</a></li>
		 <li role="presentation"><a href="{{ route('admin.reporteventas.index') }}">Ventas</a></li>
		</ul>		
	</div>
	
			<div class="page" style="background-color: #D8D8D8;">
			{!! Form::open(array('method'=>'post','route'=>'admin.stock.index')) !!}
			<div class="form-group">     
			<label for="sel11">Seleccione Todos o solo con Stock:</label>
	          <select id="cbcstock" name="cbcstock" class="form-control" >
			  <option value="0" <?php echo $seleccion1; ?>>Todos</option>
			  <option value="1" <?php echo $seleccion2; ?>>Con Stock</option>
			   </select>
			 </div> 	
					 {!! Form::submit('Consultar', array('class' => 'btn btn-primary')) !!}
					 {!! Form::close() !!}
					 
					<br>
					<h2>
					
					 <a class="btn btn-primary" href="{{ route('admin.stock.getexport') }}?cbcstock=<?=$control?>"> 
					 <i  class="fa fa-file-excel-o" style="font-size: 22px;"></i>
					 Exportar</a>
					</h2>
				<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
				    <thead>
                        <tr>
                            <th style="text-align: center;">id</th>
                            <th style="text-align: center;">nombre producto</th>
                            <th style="text-align: center;">Precio</th>
                            <th style="text-align: center;">Nombre Subproducto</th>
                            <th style="text-align: center;">Stock</th>
                        </tr>
                    </thead>
				<tbody>
				@foreach($elementos as $item)
				<tr>
				<td>{{$item->id}}</td>
				 <td>{{$item->productname}}</td>
				 <td>{{$item->price}}</td>
				<td><?php if($item->subproductname=='') echo '-'; else { echo $item->subproductname;  } ?></td>
				 <td><?php if($item->stock=='') echo '0'; else { echo $item->stock;  } ?></td>
				 </tr>
				@endforeach
				</tbody>
				</table>
				</div>
				 <?php echo $elementos->render(); ?>
				</div>

	</div>
	@stop




