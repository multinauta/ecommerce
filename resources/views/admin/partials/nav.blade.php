<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand main-title" href="{{ route('home') }}">EM Shoes</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<p class="navbar-text"><i class="fa fa-dashboard"></i><a href="home">Panel de Control</a></p>
      <ul class="nav navbar-nav navbar-right">
		<li><a href="{{ route('admin.category.index') }}">Categorias</i></a></li>
		<li><a href="{{ route('admin.product.index') }}">Productos</i></a></li>
		<li><a href="{{ route('admin.order.index') }}">Pedidos</i></a></li>
		<li><a href="{{ route('admin.user.index') }}">Usuarios</i></a></li>
			<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
			<i class="fa fa-user"></i> {{ Auth::user()->user }} <span class="caret"></span>
		</a>
		<ul class="dropdown-menu" role="menu">
			<li><a href="{{ route('logout') }}">Finalizar sesi&oacute;n</a></li>
		</ul>
	</li>
      </ul>
    </div>
  </div>
</nav>