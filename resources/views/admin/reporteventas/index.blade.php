<!DOCTYPE html>

<html lang="es">
<head>
	<meta charset="UTF-8">
<meta charset="ISO-8859-1">
	<title>@yield('title','EM Shoes')</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/lumen/bootstrap.min.css" rel="stylesheet" integrity="sha256-QSktus/KATft+5BD6tKvAwzSxP75hHX0SrIjYto471M= sha512-787L1W8XyGQkqtvQigyUGnPxsRudYU2fEunzUP5c59Z3m4pKl1YaBGTcdhfxOfBvqTmJFmb6GDgm0iQRVWOvLQ==" crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Poiret+One|Lobster+Two' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{ asset('admin/css/main.css') }}">
	
	</head>
<body>
 
    @if(\Session::has('message'))
		@include('admin.partials.message')
    @endif
	@include('admin.partials.nav')
	
	        <link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
		<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
			<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
	

	<div class="container text-center">
	
	<div class="page-header">
		<ul class="nav nav-pills">
		 <li role="presentation" ><a href="{{ route('admin.stock.index') }}">Stock</a></li>
		 <li role="presentation" class="active"><a href="{{ route('admin.reporteventas.index') }}">Ventas</a></li>
		</ul>
	</div>
	
	
	
			<div class="page" style="background-color: #D8D8D8;">
	
			<h3 class="h3">Generar Reporte de Ventas</h3>
			<hr>
{!! Form::open(array('method'=>'post','route'=>'admin.reporteventas.getexport')) !!}			
    <div class='col-md-5'>
        <div class="form-group">
			<label for="desde">Desde:</label>
            <div class='input-group date' id='datetimepicker6'>
                <input type='text' name="desde" class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
	
    <div class='col-md-5'>
        <div class="form-group">
		<label for="hasta">Hasta:</label>
            <div class='input-group date' id='datetimepicker7'>
                <input type='text' name="hasta" class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
	<br>
	
				 {!! Form::submit('Exportar', array('class' => 'btn btn-primary fa fa-file-excel-o')) !!}
					 {!! Form::close() !!}


<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker(
		{format: 'YYYY/MM/DD'}
		);
        $('#datetimepicker7').datetimepicker({
		format: 'YYYY/MM/DD',
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
				</div>

	</div>
	

<script src="{{ asset('admin/js/main.js') }}"></script>
</body>
</html>




