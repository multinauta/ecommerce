
	@extends('admin.template')

	@section('content')

		<div class="container text-center">
			<div class="page-header">
				<h1>
					<i class="fa fa-list-ol"></i>
					SUB-PRODUCTOS 
				</h1>
			</div>
			<div class="page">			
			                    @if (count($errors) > 0)
                        @include('admin.partials.errors')
                    @endif
			  {!! Form::open(array('method'=>'get','route'=>'admin.subproduct.index')) !!}
			  
		   <div class="form-group">     
			<label for="sel1">Seleccione un Producto:</label>
	          <select id="producto" name="producto" class="form-control" OnChange="ocultar_tabla_subproductos()">
			  <option value="seleccione">Seleccione</option>
	          @foreach($products as $product)
			  
			    
			  
	           
				 <?php 
				 if($postproducto==$product->id)
            {
               echo "<option value='".$product->id."' selected>".$product->name."</option>";
            }
            else
            {
               echo "<option value='".$product->id."'>".$product->name."</option>";
            }
	    ?>
	          @endforeach
	          </select>
	<br>
											
					<hr>
	<br>

	</div>
	 
				
				<hr>
				
		 {!! Form::submit('Consultar', array('class' => 'btn btn-primary')) !!}
		        <a id="btncrearsubproducto" href="{{ route('admin.subproduct.create') }}?producto=<?=$postproducto?>" class="btn btn-warning">
						<i class="fa fa-plus-circle"></i> Agregar Sub-Producto
					</a>
				 {!! Form::close() !!}
				 <br>

				 
				 
				             <div class="table-responsive">
                <table id="tabla_subproductos" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Editar</th>
                            <th>Eliminar</th>
                            <th>Nombre</th>
							<th>Stock</th>
                            <th>Comentario</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subproducts as $subproduct)
                            <tr>
                                <td>
                                    <a href="{{ route('admin.subproduct.edit', $subproduct->id) }}" class="btn btn-primary">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                </td>
                                <td>
                                    {!! Form::open(['route' => ['admin.subproduct.destroy', $subproduct->id]]) !!}
        								<input type="hidden" name="_method" value="DELETE">
        								<button onClick="return confirm('Eliminar registro?')" class="btn btn-danger">
        									<i class="fa fa-trash-o"></i>
        								</button>
        							{!! Form::close() !!}
								</td>
                                <td>{{ $subproduct->name }}</td>
								<td>{{ $subproduct->stock }}</td>
                                <td>{{ $subproduct->comment }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
				 
				 
				 
				 
				 
			</div>

		</div>

<script>
	function ocultar_tabla_subproductos()
	{
		
		$("#tabla_subproductos").hide();
		
		$("#btncrearsubproducto").hide();
	}
	
	
	
	
</script>
	@stop




