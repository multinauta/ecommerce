@extends('admin.template')

@section('content')
	
	<div class="container text-center">
		<div class="page-header">
			<h1>
				<i class="fa fa-shopping-cart"></i>
				SUB-PRODUCTOS <small>[Editar Sub-producto]</small>
			</h1>
		</div>

		<div class="row">
            <div class="col-md-offset-3 col-md-6">
                
                <div class="page">
                    
                    @if (count($errors) > 0)
                        @include('admin.partials.errors')
                    @endif
                    
                    {!! Form::model($subproduct, array('route' => array('admin.subproduct.update', $subproduct->id))) !!}
                    
                        <input type="hidden" name="_method" value="PUT">
                    

                       <div class="form-group">
                            <label for="product_id">Producto:</label>
							<br>
                            
                            {!! 
                                Form::text(
                                    'product_id', 
                                    null, 
									['readonly'],
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa Stock...',
                                    )
                                ) 
                            !!}
                        </div>
                       <div class="form-group">
                            <label for="product_id">SubProducto:</label>
							<br>
                            
                            {!! 
                                Form::text(
                                    'id', 
                                    null, 
									['readonly'],
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa Stock...',
                                    )
                                ) 
                            !!}
                        </div>        
                        <div class="form-group">
                            <label for="name">Nombre:</label>
                            
                            {!! 
                                Form::text(
                                    'name', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa el nombre...',
                                        'autofocus' => 'autofocus'
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                            <label for="stock">Stock:</label>
                            
                            {!! 
                                Form::text(
                                    'stock', 
                                    null, 
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa Stock...',
                                    )
                                ) 
                            !!}
                        </div>
                        
                        <div class="form-group">
                            <label for="comment">Comentario:</label>
                            
                            {!! 
                                Form::textarea(
                                    'comment', 
                                    null, 
                                    array(
                                        'class'=>'form-control'
                                    )
                                ) 
                            !!}
                        </div>
                        
                        
                        
                        <div class="form-group">
                            {!! Form::submit('Actualizar', array('class'=>'btn btn-primary')) !!}
                            <a href="{{ route('admin.subproduct.index') }}" class="btn btn-warning">Cancelar</a>
                        </div>
                    
                    {!! Form::close() !!}
                    
                </div>
                
            </div>
        </div>
        

	</div>

@stop