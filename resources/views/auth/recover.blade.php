@extends('store.template')

@section('content')

	<div class="container text-center">
		<div class="page-header">
			<h1><i class="fa fa-user"></i>Recuperar Contraseña</h1>
		</div>
		
		<div class="row">
			<div class="col-md-offset-2 col-md-8">
				<div class="page">
				
				@include('store.partials.errors')
				  {!! Form::open(['route' => 'recover-post','method' => 'post']) !!}
				    <div class="form-group">
						<label for="email"  style="font-weight: bold;">Ingrese el Correo asociado a la cuenta</label>
						<input class="form-control" type="email" name="email" value="{{ old('email') }}">
					</div>
					<div  class="form-group">
						<button class="btn btn-primary btn-block" type="submit">Recuperar</button>
					</div>
				 {!! Form::close() !!}
				 <label  style="font-weight: bold;">Un correo será enviado a su casilla con un link para recuperar su contraseña</label>
				</div>
			</div>
		</div>
	
	</div>

@stop
