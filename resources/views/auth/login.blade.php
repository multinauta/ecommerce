@extends('store.template')

@section('content')

	<div class="container text-center">
		<div class="page-header">
			<h1><i class="fa fa-user"></i>Iniciar sesi&oacute;n</h1>
		</div>
		
		<div class="row">
			<div class="col-md-offset-2 col-md-8">
				<div class="page">
				
				@include('store.partials.errors')
				
					<form method="POST" action="{{ url('/auth/login') }}">
    {!! csrf_field() !!}

    <div class="form-group">
       <label for="email"  style="font-weight: bold;">Email</label>
        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
    </div>

    <div class="form-group">
        <label for="password"  style="font-weight: bold;">Password</label>
        <input class="form-control"  type="password" name="password" id="password">
    </div>

    <div>
        <input type="checkbox" name="remember"> Recuerdame
    </div>

    <div  class="form-group">
        <button class="btn btn-primary btn-block" type="submit">Login</button>
    </div>
</form>
 <div  class="form-group">
<a class="btn btn-primary btn-block" href="{{  url('/auth/recover') }}"> Recuperar Contraseña</a>
    </div>				
				</div>
			</div>
		</div>
	
	</div>

@stop
