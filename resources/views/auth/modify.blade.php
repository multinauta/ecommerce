@extends('store.template')

@section('content')

	<div class="container text-center">
		<div class="page-header">
			<h1><i class="fa fa-user"></i>Recuperar Contraseña</h1>
		</div>
		
		<div class="row">
			<div class="col-md-offset-2 col-md-8">
				<div class="page">

		<div class="page-header">
			<h1><i class="fa fa-user"></i>Introduzca una nueva contraseña</h1>
		</div>
	
		<div class="row">
			<div class="col-md-offset-2 col-md-8">
				<div class="page">
				
				@include('store.partials.errors')
				  {!! Form::open(['route' => 'modify-post','method' => 'post']) !!}
				    <div class="form-group">
						<label for="password"  style="font-weight: bold;">nueva contraseña</label>
						<input id="password1" class="form-control" type="password" name="pass1" value="{{ old('email') }}" pattern="[A-Za-z0-9!?-]{4,12}" required/>
					</div>
				    <div class="form-group">
						<label for="password2"  style="font-weight: bold;">Repita nueva contraseña</label>
						<input id="password2" class="form-control" type="password" name="pass2" value="{{ old('email') }}" pattern="[A-Za-z0-9!?-]{4,12}" required/>
					</div>
					<input type="hidden" name="token" value="{{!!$token!!}}">
					<div  class="form-group">
						<button class="btn btn-primary btn-block" type="submit">Cambiar</button>
					</div>
					la contraseña debe tener entre 4 y 12 carácteres
				 {!! Form::close() !!}
				
				</div>
			</div>
				
				
				
				</div>
			</div>
		</div>
	
	</div>
	<script type="text/javascript">
window.onload = function () {
	document.getElementById("password1").onchange = validatePassword;
	document.getElementById("password2").onchange = validatePassword;
}
function validatePassword(){
var pass2=document.getElementById("password2").value;
var pass1=document.getElementById("password1").value;
if(pass1!=pass2)
	document.getElementById("password2").setCustomValidity("Contrase�as no coinciden");
else
	document.getElementById("password2").setCustomValidity('');	 
//empty string means no validation error
}
</script>

@stop
