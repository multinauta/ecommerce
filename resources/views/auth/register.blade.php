@extends('store.template')

@section('content')
	<div class="container text-center">

		<div class="page-header">
		  <h1><i class="fa fa-user"></i> Registrarse</h1>
		</div>

		<div class="row">
			<div class="col-md-offset-2 col-md-8">
				<div class="page" >

				@include('store.partials.errors')

					<form method="POST" action="{{ url('/auth/register') }}">
					    {!! csrf_field() !!}

					    <div class="form-group">
					        <label for="name"  style="font-weight: bold;">Nombre</label>
					        <input class="form-control" type="text" name="name" value="{{ old('name') }}">
					    </div>

					    <div class="form-group">
					        <label for="last_name"  style="font-weight: bold;">Apellidos</label>
					        <input class="form-control" type="text" name="last_name" value="{{ old('last_name') }}">
					    </div>

					    <div class="form-group">
					        <label for="email"  style="font-weight: bold;">Correo</label>
					        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
					    </div>

					    <div class="form-group">
					        <label for="user"  style="font-weight: bold;">Usuario</label>
					        <input class="form-control" type="text" name="user" value="{{ old('user') }}">
					    </div>
						<label style="font-weight: bold;">La contraseña debe tener entre 6 y 10 carácteres</label>
						<br>	
					    <div class="form-group">
					        <label for="password"  style="font-weight: bold;">Password</label>
					        <input class="form-control" type="password" name="password">
					    </div>

					    <div class="form-group">
					        <label for="password_confirmation"  style="font-weight: bold;">Confirmar Password</label>
					        <input class="form-control" type="password" name="password_confirmation">
					    </div>

					    <div class="form-group">
					        <label for="adrress"  style="font-weight: bold;">Direcci&oacute;n</label>
					        <textarea class="form-control" name="address">{{ old('address') }}</textarea>
					    </div>
					    <div class="form-group">
					        <label for="phone"  style="font-weight: bold;">Teléfono</label>
					        <textarea class="form-control" name="phone">{{ old('phone') }}</textarea>
					    </div>

					    <div class="form-group"  style="font-weight: bold;">
					        <button class="btn btn-primary" type="submit">Crear cuenta</button>
					    </div>
					</form>

				</div>
			</div>
		</div>
	</div>
@stop
