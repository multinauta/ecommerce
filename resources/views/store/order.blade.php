@extends('store.template')

@section('content')
    <div class="container text-center">
        <div class="page-header">
            <h1>
                <i class="fa fa-shopping-cart"></i> MIS PEDIDOS
            </h1>
        </div>
        
        <div class="page">
            
            <?php if(count($orders)>0 ) { ?>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Ver Detalle</th>
                            <th>Numero de pedido</th>
                            <th>Fecha</th>
							<th>Dirección de envío</th>
                            <th>Subtotal</th>
                            <th>Envio</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>
                                    <a 
                                        href="#" 
                                        class="btn btn-primary btn-detalle-pedido"
                                        data-id="{{ $order->id }}"
                                        data-path="{{ route('admin.order.getItems') }}"
                                        data-toggle="modal" 
                                        data-target="#myModal"
                                        data-token="{{ csrf_token() }}"
                                    >
                                        <i class="fa fa-external-link"></i>
                                    </a>
                                </td>
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->created_at->format('d/m/y H:m:s') }}</td>
								<td>{{$order->address }}</td>
                                <td>${{ number_format($order->subtotal) }}</td>
                                <td>${{ number_format($order->shipping) }}</td>
                                <td>${{ number_format($order->subtotal + $order->shipping) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <hr>
            
            <?php echo $orders->render();

            }else { ?>

                <h3><span class="label label-warning">No tienes pedidos realizados :(</span></h3>
                
            <?php } ?>
            
        </div>
    </div>

    @include('store.partials.modal-detalle-pedido')
@stop