<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
<meta charset="ISO-8859-1">
<?php if (strpos($_SERVER['REQUEST_URI'], 'product/') !== false) { ?>
    <meta property="og:url"           content="<?php echo $direccion; ?>" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="{{ $product->name }}" />
	<meta property="og:description"   content="{{ $product->description }}" />
	<meta property="og:image"         content="{{ $product->image }}" />
<?php } ?>	
	<title>@yield('title','EM Shoes')</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/lumen/bootstrap.min.css" rel="stylesheet" integrity="sha256-QSktus/KATft+5BD6tKvAwzSxP75hHX0SrIjYto471M= sha512-787L1W8XyGQkqtvQigyUGnPxsRudYU2fEunzUP5c59Z3m4pKl1YaBGTcdhfxOfBvqTmJFmb6GDgm0iQRVWOvLQ==" crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Poiret+One|Lobster+Two' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
			<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-82817005-1', 'auto');
  ga('send', 'pageview');

</script>
	</head>
<body>
 
    @if(\Session::has('message'))
		@include('store.partials.message')
    @endif
	@include('store.partials.nav')

	@yield('content')

	@include('store.partials.footer')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>	
<script src="{{ asset('js/pinterest_grid.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>