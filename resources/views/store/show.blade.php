@extends('store.template')
	
@section ('content')

<?php 
$stock=0;
foreach($subproducts as $su)

{

$stock = $stock+number_format($su->stock);

}
$direccion =  $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>
<script>

$('head').append('<meta property="og:url"           content="<?php echo $direccion; ?>" />');
$('head').append('<meta property="og:title"         content="{{ $product->name }}" />');
$('head').append('<meta property="og:description"   content="{{ $product->description }}" />');




</script>

<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>



<div class="copntainer text-center">
   <div class="page-header">
  <h1><i class="fa fa-shopping-cart"></i> Detalle del producto</h1>
  </div>

  <div class="row">
	<div class="col-md-6">
		<div class="product-block">
		<img src="{{ asset($product->image) }}">	
	</div>
	</div>
	<div class="col-md-6">
		<div class="product-block">
			<!-- Your share button code -->
	<div class="fb-share-button" 
		data-href="<?php echo $direccion; ?>" 
		data-layout="button_count" data-size="large" data-mobile-iframe="true">
		<a style="background-color: #5858FA; font-weight: bold; color: white;" class="btn btn-block btn-social btn-facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F<?php echo $direccion; ?>"><span class="fa fa-facebook"></span> Comparte en facebook</a>
	</div>
	
		<a style="background-color: #1dcaff; font-weight: bold; color: white;" class="btn btn-block btn-social btn-twitter" href="https://twitter.com/intent/tweet?url=<?php echo $direccion; ?>&text=EMSHOES/Nuevo Producto/{{ $product->name }}/ http://<?php echo $direccion; ?> " class="twitter" target="_blank">
   <span class="fa fa-twitter"></span> Comparte en Twitter
</a>




		{!! Form::open(array('route'=>'cart-add', 'method' => 'post')) !!}
		<h3>{{ $product->name }}</h3><hr>
		<input type="hidden" name="idproducto" value="{{ $product->id }}"/> 
		<input type="hidden" name="slug" value="{{ $product->slug }}"/> 
		<input type="hidden" name="product" value="{{ $product->slug }}"/> 
		<h3>Stock:<?php echo $stock;  ?></h3><hr>
			<div class="product-info panel">
				<p>{{ $product->description }}</p>
				<table id="tablades" class="table table-striped table-bordered table-hover">
				     <thead align="center">
                        <tr>
                            <th><h3>SubProducto<h3></th>
                            <th><h3>Stock<h3></th>
							<th><h3>Cantidad<h3></th>
                        </tr>
                    </thead>
					<?php $i=0; ?>
			    @foreach($subproducts as $subproduct)
				
				<tr>
				<td align="left">
				{{ $subproduct->name}}
				<input type="hidden" name="idsubproducto{{ $i}}" value="{{ $subproduct->id }}"/> 
				</td>
				<td align="left">
				{{ $subproduct->stock}}
				</td>
				<td align="left">
				<input type="hidden" name="namesubproducto{{ $i }}" value="{{ $subproduct->name }}"/> 
				<input type="number" class="txt"  name="cantidad{{ $i }}" value="" min="0" max="{{ $subproduct->stock}}"/> 
				<input type="hidden" name="largoarreglo" value="{{ $i }}"/>
				</td>
				</tr>
				<?php $i++; ?>
	            @endforeach
				
				</table>
				<h3><span class="label label-success">Precio: ${{ number_format($product->price, 0, ",", ".") }}</span></h3>
				</br>
				<?php if($stock>0){  ?>
				<h3>Total Cantidad<h3>
				<input type="number" id="sum" name="totalcantidad" value="" value="" min="1" max="1000000" readonly/> 
				</br>
				<p>
				<!--	<a class="btn btn-warning btn-block" href="{{ route('cart-add',$product->slug) }}">
						<a class="btn btn-warning btn-block" onclick="alert('Disponible Próximamente')">
					 La quiero <i class="fa fa-cart-plus fa-2x"></i>
					</a>  -->
				
					  {!! Form::submit('La Quiero', array('class'=>'btn btn-primary','id'=>'quiero')) !!}
				</p>

	

    
	<?php  }else { ?>
				<h2>No hay Stock</h2>
				<h3 style="font-family: Arial;">Para solicitud de producto contactenos a carolinapf.uv@gmail.com</h3>
				<?php } ?>
				
			</div>
			  {!! Form::close() !!}
	</div>
	</div>
  </div><hr>
  


	
	<p><a class="btn btn-primary" href="{{ route('home') }}"><i class="fa fa-chevron-circle-left"></i>Regresar</a></p>


	</div>
	
<?php 




?>

@stop
