@extends('store.template')


@section('content')
	<div class="container text-center">
		<div class="page-header">
			<h1><i class="fa fa-shopping-cart"></i>Carrito de compras</h1>
		</div>
		
		<div class="table-cart">
		 @if(count($cart))
			 <p>
				<a href="{{ route('cart-trash') }}" class="btn btn-danger">
				Vaciar carrito <i class="fa fa-trash"></i>
				</a>
			 </p>
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead style="text-align: center;">
					<tr>
						<th style="text-align: center;">Imagen</th>
						<th style="text-align: center;">Producto</th>
						<th style="text-align: center;">Detalle-Cantidad</th>
						<th style="text-align: center;">Precio</th>
						<th style="text-align: center;">Cantidad</th>
						<th style="text-align: center;">Subtotal</th>
						<th style="text-align: center;">Quitar</th>
					</tr>
				</thead>
				<tbody>
					@foreach($cart as $item)
						<tr>
							<td><img src="{{ asset($item->image) }}"></img></td>
							<td>{{ $item->name }}</td>
							<td>
							@foreach($item->detalle as $det)
							<?php $array = explode("-", $det); ?>
							<?php echo $array[1].' - '.$array[2]; ?>
							<br>
							@endforeach
							</td>
							<td>${{ number_format($item->price, 0, ",", ".") }}</td>
							<td>
								<input 
									type="number" 
									min="1" 
									max="100" 
									value="{{ $item->quantity }}" 
									id="product_{{ $item->id }}"
								>
								<a 
								href="#" 
								class="btn btn-warning btn-update-item"
								data-href="{{ route('cart-update',$item->slug) }}" 
								data-id="{{ $item->id }}" 
								>
								<i class="fa fa-refresh"></i>
								</a>
							</td>
							<td>${{ number_format($item->price * $item->quantity, 0, ",", ".") }}</td>
							<td>
								<a href="{{ route('cart-delete',$item->slug) }}" class="btn btn-danger">
									<i class="fa fa-remove"></i>
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
			<hr>
			{!! Form::open(array('method'=>'get','route'=>'order-detail')) !!}
			<div style="font-size: 150%;">
			¿Desea envíar el producto a otra dirección?
			Si&nbsp;<input type="checkbox" name="cambiar" onclick="toggle('.myClass', this)" value="true"><br>
			<textarea name="direccion" class="myClass" rows="4" cols="50" style="display: none;" placeholder="Ingrese Dirección de envío"></textarea>
			 
			</div>
			
			<h3><span class="label label-success">
			Total: ${{ number_format($total, 0, ",", ".") }}
			</span></h3>
		</div>
					 {!! Form::submit('Continuar', array('class' => 'btn btn-primary')) !!}
					 {!! Form::close() !!}
		@else
			<h3><span class="label label-warning">No hay productos en el carrito :(</span></h3>
		@endif
		<hr>
		<p>
			<a href="{{ route('home') }}" class="btn btn-primary">
				<i class="fa fa-chevron-circle-left"></i> Seguir comprando
			</a>
	<!--		<a href="{{ route('order-detail') }}" class="btn btn-primary"> -->
		<!--		Continuar <i class="fa fa-chevron-circle-right"></i>-->
		<!--	</a> -->
		</p>

		</div>
	</div>
	
	<script>
function toggle(className, obj) {
    var $input = $(obj);
    if ($input.prop('checked')) $(className).show();
    else $(className).hide();
}
</script>
@stop
