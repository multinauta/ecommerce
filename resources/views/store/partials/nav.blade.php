<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand main-title" href="{{ route('home') }}">EM Shoes</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<p class="navbar-text">Botas y Botines</p>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ route('cart-show') }}"><i class="fa fa-shopping-cart"></i></a></li>
		<?php if(($_SERVER['REQUEST_URI'])==''){ ?>
		<li><a href="admin/home">Panel de Control</i></a></li>
		<?php  } else {?>
		<li><a href="../admin/home">Panel de Control</i></a></li>
		<?php }?>
	<!--	<li><a href="#">Conocenos</i></a></li>
		<li><a href="#">Contacto</i></a></li> -->
		@include('store.partials.menu-user')
      </ul>
    </div>
  </div>
</nav>