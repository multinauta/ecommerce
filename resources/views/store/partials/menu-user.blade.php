@if(Auth::check())
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
			<i class="fa fa-user"></i> {{ Auth::user()->user }} <span class="caret"></span>
		</a>
		<ul class="dropdown-menu" role="menu">
			<li><a href="{{ route('logout') }}">Finalizar sesi&oacute;n</a></li>
			<li><a href="{{ route('admin.user2.edit', Auth::user()->id ) }}" >Administrar perfil</a></li>
			<li><a href="{{ route('admin.user4.mispedidos') }}" >Mis pedidos</a></li>
		</ul>
	</li>
	
@else
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
			<i class="fa fa-user"></i> <span class="caret"></span>
		</a>
		<ul class="dropdown-menu" role="menu">
			<li><a href="{{ route('login-get') }}"  style="font-weight: bold;">Iniciar sesi&oacute;n</a></li>
			<li><a href="{{ route('register-get') }}"  style="font-weight: bold;">Registrarse</a></li>
			<li><a href="{{ route('recover-get') }}"  style="font-weight: bold;">Recuperar Contraseña</a></li>
 
		</ul>
	</li>

@endif