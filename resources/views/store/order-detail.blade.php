@extends('store.template')

@section('content')
	
	<div class="container text-center">
		<div class="page-header">
			<h1><i class="fa fa-shopping-cart"></i> Detalle del pedido</h1>
		</div>

		<div class="page">
			<div class="table-responsive">
				<h3>Datos del usuario</h3>
				<table class="table table-striped table-hover table-bordered">
					<tr><td>Nombre:</td><td>{{ Auth::user()->name . " " . Auth::user()->last_name }}</td></tr>
					<tr><td>Usuario:</td><td>{{ Auth::user()->user }}</td></tr>
					<tr><td>Correo:</td><td>{{ Auth::user()->email }}</td></tr>
					<tr><td>Direcci&oacute;n:</td><td>{{ Auth::user()->address }}</td></tr>
				</table>
			</div>
			<div class="table-responsive">
				<h3>Datos del pedido</h3>
				<table class="table table-striped table-hover table-bordered">
					<tr>
						<th style="text-align:center;">Producto</th>
						<th style="text-align:center;">Detalle-Cantidad</th>
						<th style="text-align:center;">Precio</th>
						<th style="text-align:center;">Cantidad</th>
						<th style="text-align:center;">Subtotal</th>
					</tr>
					@foreach($cart as $item)
						<tr>
							<td>{{ $item->name }}</td>		
							<td>
							@foreach($item->detalle as $detalle)
							    <?php 
								$array = array();
								$array = explode("-", $detalle);
								echo $array[1];
								echo "-";
								echo $array[2];
								?>
								
								<br>
							@endforeach
							</td>
							<td>${{ number_format($item->price) }}</td>
							<td>{{ $item->quantity }}</td>
							<td>${{ number_format($item->price * $item->quantity) }}</td>
						</tr>
					@endforeach
				</table><hr>
				<h3>
					<span class="label label-success">
						Total: ${{ number_format($total) }}
					</span>
				</h3><hr>
				<!-- Formulario HTML que envía la Nueva Orden -->
    		<form method="POST" action="{{ config('flow.url_pago') }}" accept-charset="UTF-8">

        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
        		<input type="hidden" name="parameters" value="{{ $flow_pack }}">
      
  
				<p>

					<button type="submit" class = "btn btn-warning">Pagar con Flow <i class="fa fa-credit-card fa-2x"></i></button>
				</p>
			  </form>
				  <p>
				  <?php
		            // Cargamos el servicio para crear el boton
		            $khipu_service = Khipu::loadService('CreatePaymentPage');
		            $payer_email = Auth::user()->email;

		            $picture_url = 'https://s3.amazonaws.com/static.khipu.com/buttons/100x50.png';

		            $data = array(
		              'subject' => 'Pago de orden',
		              'body' => 'Orden N° '. $orden_compra,
		              'amount' => $total,
		              'transaction_id' => $orden_compra,
		              // Dejar por defecto un correo para recibir el comprobante
		              'payer_email' => $payer_email,
		              // url de la imagen del producto o servicio
		              'picture_url' => $picture_url,
		              // Opcional
		              'custom' => '',
		              // definimos una url en donde se notificará del pagos
		              'notify_url' => asset('khipu/confirmacion'),
		              'return_url' => asset('khipu/exito'),
		              'cancel_url' => asset('khipu/fracaso'),
		            );
		           


			      	foreach ($data as $name => $value):

			        	// Le asignamos los valores
			        	$khipu_service->setParameter($name, $value);
			        
			    	endforeach;

			        // Generamos el formulario.
			        print $khipu_service->renderForm("150x50");

		          ?>
		          </p>
		          <hr>
		          <p>
		          	<a href="{{route('cart-show')}}" class="btn btn-primary"><i class="fa fa-chevron-circle-left"></i> Regresar</a>
		          </p>
		          					
			</div>
		</div>
	</div>

	
@stop
