@extends('store.template')

@section('content')

@include('store.partials.slider')



	
<div class="container text-center">

<div class="row">

  <div class="col-lg-6 pull-right">
  {!! Form::open(['route' => 'home','method' => 'get']) !!}
    <div class="input-group">
	  
      <input name="busqueda" type="text" class="form-control" placeholder="Búsqueda de Producto...">
      <span class="input-group-btn">
        <button class="btn btn-primary" type="submit">Buscar</button>
		
      </span>
	 
    </div><!-- /input-group -->
	 {!! Form::close() !!}
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->
<hr>
<div class="row">
<div class="col-md-2">
<h3>Categorías<h3>
<ul class="nav nav-pills nav-stacked">
 <li role="presentation"><a href="{{ route('home' ,'categoria=todas') }}">Todas</a></li>
  @foreach($categorias as $categoria)
  <li role="presentation"><a href="{{ route('home' ,'categoria='.$categoria->id) }}">{{ $categoria->name }}</a></li>
  @endforeach
</ul>
</div>
<div id="products" class="col-md-10">
	@foreach($products as $product)
	
 @if ($product->visible=='1')
		<div class="product white-panel">
			<h3>{{$product->name}}</h3><hr>
			<img src="{{ $product->image }}" width='200'>
			<div class="product-info panel">
			  <p>{{ $product->extract}}</p>
			  <h3><span class="label label-success">Precio: ${{ number_format($product->price, 0, ",", ".") }}</span></h3>
			  <p>
		<!--		<a class="btn btn-warning" href="{{ route('cart-add',$product->slug) }}">  -->
		<!--		<a class="btn btn-warning" onclick="alert('Disponible próximamente')"> -->
		<!--		<i class="fa fa-cart-plus"></i> La quiero</a> -->
				<a class="btn btn-primary" href="{{ route('product-detail' , $product->slug) }}"><i class="fa fa-chevron-circle-right"></i> Leer mas</a>
			  </p>
			</div>
		</div>
		
	@endif
	@endforeach
</div>
</div>
<?php echo $products->render(); ?>
</div>
<hr>
@stop
