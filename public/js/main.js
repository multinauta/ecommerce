

$(document).ready(function() {


$(".btn-detalle-pedido").on('click', function(e){
    e.preventDefault();

    var id_pedido = $(this).data('id');
        var path = $(this).data('path');
        var token = $(this).data('token');
        var modal_title = $(".modal-title");
        var modal_body = $(".modal-body");
        var loading = '<p><i class="fa fa-circle-o-notch fa-spin"></i> Cargando datos</p>';
        var table = $("#table-detalle-pedido tbody");
        var data = {'_token' : token, 'order_id' : id_pedido};

        modal_title.html('Detalle del Pedido: ' + id_pedido);
        table.html(loading);
 
    
        $.post(
          path,
          data,
          function(data){
            //console.log(response);
            table.html("");
            
                for(var i=0; i<data.length; i++){
                    
                    var fila = "<tr>";
                    fila += "<td><img src='../" + data[i].product.image + "' width='30'></td>";
                    fila += "<td>" + data[i].product.name + "</td>";
                    fila += "<td>$ " + numberWithCommas(parseInt(data[i].price)) + "</td>";
                    fila += "<td>" + parseInt(data[i].stock) + "</td>";
                    fila += "<td>$ " + numberWithCommas((parseInt(data[i].quantity) * parseInt(data[i].price))) + "</td>";
          fila += "<td>" + data[i].detail + "</td>";
                    fila += "</tr>";
                    
                    table.append(fila);
                }s
          },
          'json'
        );

  });

$('#products').pinterest_grid({
no_columns: 4,
padding_x: 10,
padding_y: 10,
margin_bottom: 50,
single_column_breakpoint: 700
});


// update item cart

$(".btn-update-item").on('click',function(e){
	e.preventDefault();
	var id= $(this).data('id');
	var href = $(this).data('href');
	var quantity = $("#product_"+id).val();
	
	window.location.href = href + "/" + quantity;
});
	

	
        //iterate through each textboxes and add keyup
        //handler to trigger sum event
		$('input[name=totalcantidad]').val('0');
		$('.txt').val('0');
        $(".txt").each(function() {
 
            $(this).keyup(function(){
                calculateSum();
            });
			 $(this).change(function(){
                calculateSum();
            });
        });
    function calculateSum() {
 
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".txt").each(function() {
 
            //add only if the value is number
            if(!isNaN(this.value) && this.value.length!=0) {
                sum += parseFloat(this.value);
            }
 
        });
        //.toFixed() method will roundoff the final sum to 2 decimal places
     //   $("#sum").html(sum.toFixed(0));
		
		 $('input[name=totalcantidad]').val(sum.toFixed(0));
    }

	
	


  $('#quiero').prop("disabled",true);

   $( ".txt" ).change(function() {
  if($( "#sum" ).val()!='0')
  {
	  $('#quiero').prop("disabled",false);
  }
  else
  {
	  $('#quiero').prop("disabled",true);
  }

});


});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}