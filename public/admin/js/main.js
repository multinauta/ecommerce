$(document).ready(function(){


function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}

	$(".btn-detalle-pedido").on('click', function(e){
		e.preventDefault();

		var id_pedido = $(this).data('id');
        var path = $(this).data('path');
        var token = $(this).data('token');
        var modal_title = $(".modal-title");
        var modal_body = $(".modal-body");
        var loading = '<p><i class="fa fa-circle-o-notch fa-spin"></i> Cargando datos</p>';
        var table = $("#table-detalle-pedido tbody");
        var data = {'_token' : token, 'order_id' : id_pedido};

        modal_title.html('Detalle del Pedido: ' + id_pedido);
        table.html(loading);
 
		
        $.post(
        	path,
        	data,
        	function(data){
        		//console.log(response);
        		table.html("");
        		
                for(var i=0; i<data.length; i++){
                    
                    var fila = "<tr>";
                    fila += "<td><img src='../" + data[i].product.image + "' width='30'></td>";
                    fila += "<td>" + data[i].product.name + "</td>";
                    fila += "<td>$ " + numberWithCommas(parseInt(data[i].price)) + "</td>";
                    fila += "<td>" + parseInt(data[i].stock) + "</td>";
                    fila += "<td>$ " + numberWithCommas((parseInt(data[i].quantity) * parseInt(data[i].price))) + "</td>";
					fila += "<td>" + data[i].detail + "</td>";
                    fila += "</tr>";
                    
                    table.append(fila);
                }s
        	},
        	'json'
        );

	});

		// para ocultar el boton de subproductos
var producto1 = $_GET('producto');

if (producto1=="null")
{
$('#btncrearsubproducto').hide();
}
else
{
	$('#btncrearsubproducto').show();
}

   $( "#producto" ).change(function() {
//	$('#btncrearsubproducto').show();
	
	if ($( "#producto" ).val()=="seleccione")
	{
		$('#btncrearsubproducto').hide();
	}
	
	});

});


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
