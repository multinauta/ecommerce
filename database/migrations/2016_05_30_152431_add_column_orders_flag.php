<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOrdersFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        	 Schema::table('orders', function (Blueprint $table) {
        $table->string('flag',1);
		
		 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        	Schema::table('orders', function (Blueprint $table) {
         	$table->dropColumn('flag');
			});
    }
}
