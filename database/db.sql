-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: 13987816
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (5,'Botas','botas','Botas','#ff0000'),(6,'Botines','botines','Botines','#0000ff'),(7,'Cinturones','cinturones','Cinturones de cuero doble hebilla y simples en diferentes colores','Negro,azul y camel'),(8,'Bolsos y carteras','bolsos-y-carteras','Hermosas carteras, mochilas y morrales full tendencia','Negro, beige, animal print , camel y azul'),(9,'Calzado hombre','calzado-hombre','Hermosos mocasines de cuero','Negro, café, camel , café moro '),(10,'Sandalias','sandalias','Bellas sandalias elaboradas con los mejores cueros y artesanos de la region metropolitana. Estilos diversos presentando diseños que van desde lo clásico a lo más chic.','Negro, azul, dorado, plateado, camel y otros');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_02_18_192647_create_categories_table',1),('2016_02_18_192718_create_products_table',1),('2016_02_22_133821_migrations',1),('2016_02_22_180810_create_orders_table',2),('2016_02_22_180841_create_order_items_table',2),('2016_04_27_095709_add_column_productos_stock',3),('2016_04_27_130748_create_subproducts_table',4),('2016_05_20_121438_create_sub_orders_table',5),('2016_05_30_105955_add_column_orderitem_address',6),('2016_05_30_133047_add_column_user_phone',7),('2016_05_30_152431_add_column_orders_flag',8),('2016_07_14_220538_add_column_orders_orderid_statusid',9);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_product_id_foreign` (`product_id`),
  KEY `order_items_order_id_foreign` (`order_id`),
  CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` VALUES (1,1,65000.00,161,1),(2,1,999.99,162,2),(3,1,85000.00,158,3),(4,1,90000.00,164,4),(5,1,90000.00,164,5),(6,1,90000.00,164,6),(7,1,90000.00,164,7),(8,1,85000.00,158,8),(9,1,85000.00,158,9),(10,1,85000.00,158,10),(11,1,90000.00,164,11),(12,2,65000.00,161,12),(13,1,85000.00,158,13),(14,1,68000.00,163,14),(15,1,65000.00,161,15),(16,1,65000.00,161,16),(17,1,65000.00,179,17),(18,1,65000.00,179,18);
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subtotal` decimal(10,2) NOT NULL,
  `shipping` decimal(10,2) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `address` varchar(999) COLLATE utf8_unicode_ci NOT NULL,
  `flag` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,65000.00,100.00,21,'2016-07-13 21:20:32','2016-07-13 21:20:32','Ecuador #1150 villa alemana','1',0,1),(2,49990.00,100.00,1,'2016-08-02 02:08:53','2016-08-02 02:08:53','sanpablo 2002 santiago','1',120160801220853,0),(3,85000.00,100.00,22,'2016-08-18 18:26:10','2016-08-18 18:27:21','Amapolas 1500 depto 304 Providencia','1',2220160818142610,1),(4,90000.00,100.00,1,'2016-08-22 13:41:41','2016-08-22 13:41:41','sanpablo 2002 santiago','1',120160822094141,0),(5,90000.00,100.00,1,'2016-08-27 23:41:24','2016-08-27 23:41:24','sanpablo 2002 santiago','1',120160827194124,0),(6,90000.00,100.00,1,'2016-08-27 23:47:22','2016-08-27 23:47:22','sanpablo 2002 santiago','1',120160827194722,0),(7,90000.00,100.00,1,'2016-08-28 00:10:28','2016-08-28 00:10:28','sanpablo 2002 santiago','1',120160827201028,0),(8,85000.00,100.00,23,'2016-08-30 18:24:11','2016-08-30 18:24:11','peñablanca 372','1',2320160830142411,0),(9,85000.00,100.00,23,'2016-08-30 18:52:58','2016-08-30 18:52:58','peñablanca 372','1',2320160830145258,0),(10,85000.00,100.00,23,'2016-08-30 19:43:17','2016-08-30 19:43:17','peñablanca 372','1',2320160830154317,0),(11,90000.00,100.00,1,'2016-08-30 21:20:13','2016-08-30 21:20:13','sanpablo 2002 santiago','1',120160830172013,0),(12,130000.00,100.00,22,'2016-08-31 18:05:04','2016-08-31 18:08:54','Amapolas 1500 depto 304 Providencia','1',2220160831140504,1),(13,85000.00,100.00,23,'2016-08-31 22:10:52','2016-08-31 22:13:00','peñablanca 372','1',2320160831181052,1),(14,68000.00,100.00,14,'2016-09-03 19:30:48','2016-09-03 19:30:48','','1',1420160903153048,0),(15,65000.00,100.00,14,'2016-09-03 20:25:56','2016-09-03 20:25:56','','1',1420160903162556,0),(16,65000.00,100.00,21,'2016-09-28 14:58:30','2016-09-28 15:01:12','Ecuador #1150 villa alemana','1',2120160928105830,1),(17,65000.00,100.00,1,'2016-10-11 14:51:53','2016-10-11 14:51:53','','',120161011105153,0),(18,65000.00,100.00,1,'2016-10-31 00:20:43','2016-10-31 00:20:43','sanpablo 2002 santiago','',120161030202043,0);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `extract` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `products_category_id_foreign` (`category_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (34,'Rafaela','rafaela','Botín en gamuza y cuero tradicional, con amarra trasera y tacón de madera. Se destaca por la comodidad y versatilidad del calzado.','Botín taco madera',65000,'img/catalogo/23.jpg',1,6,'0000-00-00 00:00:00','2016-04-19 01:59:53',0),(40,'Rosalía','rosalia','Bota  plana  ( 3 centímetros de suela) en gamuza aterciopelada color chocolate, con detalle de hebilla en un costado.','Bota plana en gamuza',85000,'img/catalogo/29.jpg',1,5,'0000-00-00 00:00:00','2016-04-19 01:57:18',0),(56,'Quarter','quarter','Bota caña larga en color chocolate, con detalle en tachas. Plataforma alta comodidad en madera trabajada.','Bota caña larga en cuero',85000,'img/catalogo/45.jpg',1,5,'0000-00-00 00:00:00','2016-04-19 01:53:10',0),(71,'Easter','easter','Bota de caña larga en material gamuza aterciopelado, con plataforma de madera trabajada. Cierre metalizado en un costado.','Bota caña larga en gamuza aterciopelada/ sin stock/ consultar nuevo arribo',85000,'img/catalogo/60.jpg',1,5,'0000-00-00 00:00:00','2016-04-19 01:50:48',0),(85,'Street','street','Botín corto en gamuza aterciopelada, con planta goma de 3 cms. Incluye correas accesorias removibles.','botín street style',65000,'img/catalogo/74.jpg',1,6,'0000-00-00 00:00:00','2016-06-05 01:22:03',0),(88,'Oliva','oliva','Botín en cuero, con cierre  posterior en talón. Plataforma de 8cms en madera. Disponible en gamuza y cuero tradicional.','Botín con plataforma madera',65000,'img/catalogo/77.jpg',1,6,'0000-00-00 00:00:00','2016-04-19 00:50:04',0),(96,'Moss','moss','Botín en cuero tradicional y gamuza aterciopelada, con detalle de cierre en un costado. Plataforma de madera seccionada.','Botín con detalle cierres',65000,'img/catalogo/85.jpg',1,6,'0000-00-00 00:00:00','2016-04-19 00:39:57',0),(105,'Dakota','dakota','Botín en gamuza aterciopelada con diseño y amarras. Planta en goma con sistema antileslizante.','Botín cuero en color rojizo',65000,'img/catalogo/94.jpg',1,6,'0000-00-00 00:00:00','2016-04-19 00:06:48',0),(115,'Print','print','Botín caña baja  en color caramelo, con diseño print en parte delantera y trasera.  Plataforma de goma con tacón grueso. Cierre de metal interno. Forrada.','Botín de cuero taco tradicional en goma',65000,'img/catalogo/104.jpg',1,6,'0000-00-00 00:00:00','2016-04-19 00:00:43',0),(117,'Flerry','flerry','Botín caña media en gamuza aterciopelada en color chocolate. Terraplen en madera trabajada a mano, con altura de 8 cms. Interior forado.','Botín de gamuza',68900,'img/catalogo/106.jpg',1,6,'0000-00-00 00:00:00','2016-06-05 01:07:36',0),(119,'Army rock','army-rock','Botín de charol disponible en color negro y vino.  Cierre lateral en color plata. Botín con correas  de charol adornadas con tachas metálicas. Plataforma de 8 cms. en madera trabajada que entrega estabilidad y seguridad al caminar.','Botín de charol/ colores negro-café-vino',68990,'img/catalogo/108.jpg',1,6,'0000-00-00 00:00:00','2016-06-05 01:06:03',0),(120,'Blue','blue','Botín de gamuza con planta de goma y accesorios en correas de cueros tono tricolor-','Botín de gamuza en color azulino',65000,'img/catalogo/109.jpg',1,6,'0000-00-00 00:00:00','2016-06-05 01:23:13',0),(122,'Luckie','luckie','Bota caña alta elaborada en richato (cuero brilloso) en color café. Cierre lateral, planta de goma y punta redonda.  Taco de aproximadamente 3 centimentos. Forrada.','bota de montar/ chocolate/negra',85000,'img/catalogo/111.jpg',1,5,'0000-00-00 00:00:00','2016-06-05 01:09:03',0),(126,'balleriana','balleriana','Botín de gamuza y richato en color negro. Presenta un cierre lateral y tacón de madera estilo chino. Disponible en color negro, taupe y chocolate.','botín taco chino',68990,'img/catalogo/115.jpg',1,6,'0000-00-00 00:00:00','2016-06-05 01:08:13',0),(133,'Quilt','quilt','Panchita de gamuza aterciopelada en color negro, con plataforma de goma en color blanco. Disponibles en tonos cafe y animal print.','zapatilla estilo panchita con flecos',49900,'img/catalogo/122.jpg',1,6,'0000-00-00 00:00:00','2016-05-30 21:41:27',0),(134,'Ester','ester','Modelo bucanera de material gamuza aterciopelada y extractos de richato en parte baja de la bota. Tiene un tacón estilo chino de madera con una altura de 8 cms de alto.','Bucanera  con tacón de madera/  Colores negro y taupe',95000,'img/catalogo/123.jpg',1,5,'0000-00-00 00:00:00','2016-05-30 21:00:31',0),(144,'Ariela','ariela','Botín de cuero graso, caña media, cierre al costado y flecos laterales. Taco chino de madera de 8 cms de altura. Este modelo se encuentra disponible en color caramelo, taupe y negro.','Botín Flecos/ colores negro-caramelo-taupe',68990,'img/catalogo/133.jpg',1,6,'0000-00-00 00:00:00','2016-06-05 01:05:41',0),(149,' Amalia','amalia','Bucanera de gamuza aterciopelada  de color negro con tacón de goma antideslizante.','Bucanera  con  tacón de goma',95000,'img/catalogo/138.jpg',1,5,'0000-00-00 00:00:00','2016-05-30 20:58:19',0),(153,' Urbano','urbano','bota de cuero en color marengo y café moro. Suela de goma antideslizante. Comodidad y estilo en una bota. Entrega perfecta protección para días de lluvia.','Bota caña larga de cuero',75000,'img/catalogo/142.jpg',1,5,'0000-00-00 00:00:00','2016-04-18 21:54:22',0),(156,'Isidorita','isidorita','botín de cuero, con aplicaciones en gamuza aterciopelada y detalle de cierre en el costado. Plataforma de 8 cms. que entrega gran comodidad y estabilidad por la forma del tacón.','botín de cuero con suela de madera',69990,'img/catalogo/145.jpg',1,6,'0000-00-00 00:00:00','2016-05-30 21:37:33',0),(158,'Elisa','elisa','botas de montar  en cuero 100% nacional, con detalle de color cobre en franja superior y hebilla en color cobre.','botas de montar con detalle en color cobre',85000,'img/catalogo/147.jpg',1,5,'0000-00-00 00:00:00','2016-04-18 21:55:10',0),(159,'Emilia','emilia','Bucanera 100% cuero  tipo graso de color caramelo con taco chino de madera de 8 cms de altura.','Bucanera de cuero graso / colores negro y caramelo',95000,'img/catalogo/136.jpg',1,5,'2016-04-18 03:16:41','2016-05-30 20:56:06',0),(160,'Daysi ','daysi','Botín de cuero en tonalidades doradas con correas desmontables y plataforma de goma que otorga gran comodidad en el calzado.','Botín cuero con correas desmontables/ disponible en dorado y plateado',68990,'img/catalogo/daysi-dorado74qictd3hr18b11b2pu93wf87ka6vngy6579mlxdj0ede5zos.jpg',1,6,'2016-04-25 04:05:31','2016-06-05 00:57:36',0),(161,'Aragán','aragan','Botín de cuero con plataforma en goma, disponibles en colores beige, café chocolate y negro.','Botín de cuero con plataforma en goma',65000,'img/catalogo/aragane07b33v1e4idcupa8fo9c3s1th7r2g80ny5b5wmjdzebq6lxk.jpg',1,6,'2016-04-25 04:15:55','2016-04-27 15:32:06',0),(162,'Alba','alba','Zapatilla estilo panchita con caña alta y suela de goma de 5 cms app.','Panchita caña alta/Dorado-plateado',49990,'img/catalogo/albaupccmzn761ed1g2aoqs9ba54yh85fw1kj89drt0ix74e3v3l3.jpg',1,6,'2016-05-30 21:23:26','2016-05-30 21:41:52',0),(163,'Hippie chic botín','hippie-chic-botin','Hermoso botín en gamuza aterciopelada con detalle de flecos y hebilla. Tacón de goma con altura de 8 cms. app.','Botín en gamuza aterciopelada / color negro-caramelo',68000,'img/catalogo/hippie-chic-botink2jgqdc45xrc6ay38f1b47n6a5o6749lvsa7eh2mwtp0fziu7.jpg',1,6,'2016-05-30 21:28:22','2016-09-03 19:29:28',0),(164,'bucanera Anastasia','bucanera-anastasia','Modelo bucanera en gamuza aterciopelada con detalle de cordones parte trasera. Además tiene incorporado forro de cuero , por lo que se puede doblar hacia afuera para cambiar apariencia de la bota.','Bucanera con tacón de goma/ Color negro y chocolate',90000,'img/catalogo/bucanera-anastasia8l192acph3q77644mx0jrz15k4e01b65vwocsdat9yuga4fni.jpg',1,6,'2016-05-30 21:35:29','2016-06-05 00:56:09',0),(172,'Mocasin','mocasin','Disponible en negro, beige y café moro','Zapato estilo masculino',68000,'img/catalogo/mocasinbcn52d5eyeb7gr742lp1ovafsh65uj0ct9w8xifmk83zeqc06.jpg',1,6,'2016-08-30 18:10:06','2016-08-30 18:10:06',0),(173,'Loretta','loretta','Hermoso botin con plataforma de\r\nGoma disponibles en colores terracota,café y negro. Cuenta con un detalle de correas en cuero desmontables, entregándole versatilidad.','Botin de cuero gamuza con suela de goma',68000,'img/catalogo/lorettal9cwo0eits1u9rbjexcvyf6hm5pe7gn1310zd8c25q71k4ca4.jpg',1,6,'2016-09-06 00:58:41','2016-09-06 00:58:41',0),(174,'Soledad','soledad','Cinturón doble hebilla disponibles en colores negros, azul y café ','Cinturón doble hebilla',20000,'img/catalogo/soledadlm0e5jw473xnhqdo7rvpfyb9gc8dzt8d1idk8259e6b3au0s9.jpg',1,7,'2016-09-30 02:40:24','2016-09-30 02:40:24',0),(175,'Lola','lola','Disponibles en todos los números.','Sandalia con  plataforma de madera',65000,'img/catalogo/lola51v09kw1co7hd1ez0dlryxqc9a6a25ngiu477b83ffpmsjta3.jpg',1,10,'2016-10-03 00:46:40','2016-10-03 00:46:40',0),(176,'Ellis','ellis','Incorpora un mini bolso','Cartera de cuero ',65000,'img/catalogo/ellisb4r778me01nau57489326y2lhtcfpcoqkdxf3szcaiv1gjw5b.jpg',1,8,'2016-10-03 00:49:59','2016-10-03 00:49:59',0),(177,'Lorenza','lorenza','Sandalia con plataforma de 8 cms de alto que otorga comodidad y seguridad en el caminar. Color café caramelo.','Sandalia de cuero con plataforma de madera',68000,'img/catalogo/lorenza1fhwftucaof5n2pczl676s093dg7a04mq25ceb8k36xvryij1.jpg',1,10,'2016-10-03 01:07:02','2016-10-03 01:07:02',0),(178,'Mona','mona','Sandalia con alto estándares de comodidad otorgada por el material de goma de la plataforma. Además cuenta con sistema antideslizante.','Sandalia con correa lateral y tacón de goma',60000,'img/catalogo/monaql0inh3y1jobcf5es7takzuv7xfrad962ff59wad1c84pgm25.jpg',1,10,'2016-10-03 01:10:18','2016-10-03 01:10:18',0),(179,'Estela','estela','Sandalia de cuero con aplicaciones en animal print.','Sandalia con plataforma de madera',65000,'img/catalogo/estelax13rsd8o00vuthgb670ia1e47zbf4bljc13ew575kmy2qfn9p.jpg',1,10,'2016-10-03 01:14:12','2016-10-03 01:14:12',0),(180,'Amaranta','amaranta','Sandalia con amarras caña alta y tacón símil madera que entrega mayor comodidad y liviandad al caminar.','Sandalia estilo romano',68000,'img/catalogo/amarantafxwef72n000qs5986ck2o965tcph79j3y48dimbg63uazrl1v.jpg',1,10,'2016-10-14 00:42:36','2016-10-14 00:42:36',0),(181,'Nakar','nakar','Disponibles en color caramelo y negro. Cuenta con bolsillos internos, broches y completamente forrado en cuero.','Bolso en forma cuadrado de cuero',68000,'img/catalogo/nakar21oavz5a825cw7qi0clx90g0jbhedcm463npuk1sf26c8r5yt.jpg',1,8,'2016-10-14 00:52:38','2016-10-14 00:52:38',0),(182,'Trébol ','trebol','Disponible en color negro y caramelo.','Cartera 100% cuero ',68000,'img/catalogo/trebol9gk5i0621ur73bh46v4tjan9oed02l58sp7bqyzwf8ed0xcm1.jpg',1,8,'2016-10-14 00:57:08','2016-10-14 00:57:08',0),(183,'Vickra','vickra','Zapato de cuero con aplicaciones de flecos y hebilla. Disponibles es cuero tradicional y gamuza. Colores: negro, azul y caramelo.','Mocasín masculino',68000,'img/catalogo/vickrab7h51xe18jedr939u2nm6wqalvi5tc3pcy0fgsozed918745k.jpg',1,10,'2016-11-04 23:02:49','2016-11-04 23:02:49',0),(184,'Emmi','emmi','Sandalia con plataforma de 7 cms de alto, símil madera que entrega liviandad y comodidad al calzar','Sandalia de cuero con correas cruzadas ',65000,'img/catalogo/emmi0l0qd2adbk1eaup8m4zsf17eh72x9g5v8ontja36wcri45y14.jpg',1,10,'2016-11-04 23:05:14','2016-11-04 23:05:14',0),(185,'Elena','elena','Sandalia con correa lateral y tacón de madera en plataforma. Disponibles en colores dorados y plateados.','Sandalia en colores Satinados.',60000,'img/catalogo/elenaz5n769dquaiceme5t8o1crv1ljwyfd31gx1bks8p4c92304h0.jpg',1,10,'2016-11-04 23:07:58','2016-11-14 02:05:07',0),(186,'Cramberry','cramberry','Sandalis de cuero con detalle en talón, y plataforma símil cuero que entrega liviandad y comodidad al caminar.\r\n','Sandalia aberturas talón ',65000,'img/catalogo/cramberrydmeu1ywig5rajv8t5fx607b283z645lhp1qodsk9an1d8fc67.jpg',1,10,'2016-11-04 23:12:55','2016-11-14 02:04:34',0),(187,'Avarla','avarla','Sandalia sin correas y planta de goma que entrega comodidad al caminar. Disponible en color uva, negro y coral.','Sandalia playera con cuatro correas',65000,'img/catalogo/avarla8u8daw7jki45vo68rpxn5q625ld1t0eg93f1h1days01cbz6m.jpg',1,10,'2016-11-04 23:16:33','2016-11-04 23:16:33',0),(188,'Amatista','amatista','Zapatos con aplicaciones de flequillos disponibles en color negro camel y azul.  ( cuero y gamuza)','Zapatos cuero con aplicación de hebillas',59990,'img/catalogo/amatistah10l74mp3v849sjzqrb1ay4dt8nf2kc85ux2i92b9o4we5g63.jpg',1,6,'2016-11-14 02:02:44','2016-11-14 02:02:44',0),(189,'Flora','flora','Sandalia elaborada en cuero con taco  goma Eva que entrega máxima comodidad al caminar. Disponible en color negro, plata y dorado.','Sandalia diseñada en cuero',59990,'img/catalogo/florax3frvyl1n1qa5k589uejmb9w8t16b6c80h7pos2zc62gi4adf.jpg',1,10,'2016-11-14 02:08:54','2016-11-14 02:08:54',0),(190,'Marina ','marina','Sandalis disponible en color negro, uva y coral','Sandalia con plataforma de goma ',58000,'img/catalogo/marinax5spc98qhb5o3z4vn8tgf4yre2umdd3a98ck1i1062j7d0lwa.jpg',1,10,'2016-11-14 02:13:36','2016-11-14 02:13:36',0);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_orders`
--

DROP TABLE IF EXISTS `sub_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_item_id` int(10) unsigned NOT NULL,
  `detail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `sub_orders_ordert_item_id_foreign` (`order_item_id`),
  CONSTRAINT `sub_orders_ordert_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_orders`
--

LOCK TABLES `sub_orders` WRITE;
/*!40000 ALTER TABLE `sub_orders` DISABLE KEYS */;
INSERT INTO `sub_orders` VALUES (1,1,'talla 38',1,'','2016-07-13 21:20:32','2016-07-13 21:20:32'),(2,2,'talla 38',1,'','2016-08-02 02:08:53','2016-08-02 02:08:53'),(3,3,'talla 37',1,'','2016-08-18 18:26:10','2016-08-18 18:26:10'),(4,12,'talla 37',2,'','2016-08-31 18:08:54','2016-08-31 18:08:54'),(5,13,'talla 36',1,'','2016-08-31 22:13:00','2016-08-31 22:13:00'),(6,16,'talla 37',1,'','2016-09-28 15:01:12','2016-09-28 15:01:12');
/*!40000 ALTER TABLE `sub_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subproducts`
--

DROP TABLE IF EXISTS `subproducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subproducts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `subproducts_product_id_foreign` (`product_id`),
  CONSTRAINT `subproducts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subproducts`
--

LOCK TABLES `subproducts` WRITE;
/*!40000 ALTER TABLE `subproducts` DISABLE KEYS */;
INSERT INTO `subproducts` VALUES (12,164,'talla 37',1,'','2016-06-04 22:28:20','2016-06-04 22:33:48'),(13,163,'talla 37',1,'','2016-06-04 23:12:16','2016-06-04 23:12:16'),(14,163,'talla 38',1,'','2016-06-04 23:14:28','2016-06-04 23:14:28'),(15,162,'talla 36',1,'','2016-06-04 23:53:16','2016-06-04 23:53:16'),(16,162,'talla 37',1,'','2016-06-04 23:54:07','2016-06-04 23:54:07'),(17,162,'talla 38',0,'','2016-06-04 23:58:38','2016-06-04 23:58:38'),(18,162,'talla 39',2,'','2016-06-05 00:01:48','2016-10-19 18:07:39'),(19,161,'talla 37',1,'','2016-06-05 00:02:33','2016-09-03 20:24:50'),(20,164,'talla 38',1,'','2016-06-05 00:03:00','2016-06-05 00:03:00'),(21,160,'talla 37',1,'','2016-06-05 00:03:43','2016-06-05 00:03:43'),(22,158,'talla 36',0,'','2016-06-05 00:04:35','2016-06-05 00:04:35'),(23,158,'talla 38',0,'','2016-06-05 00:06:20','2016-06-05 00:06:20'),(24,158,'talla 37',0,'','2016-06-05 00:07:09','2016-06-05 00:07:09'),(25,158,'talla 39',0,'','2016-06-05 00:07:46','2016-06-05 00:07:46'),(26,144,'talla 37',1,'','2016-06-05 00:25:18','2016-06-05 00:25:18'),(27,144,'talla 38',1,'','2016-06-05 00:26:03','2016-06-05 00:26:03'),(28,164,'talla 39',1,'','2016-06-05 00:26:33','2016-06-05 00:26:33'),(29,133,'talla 37',1,'','2016-06-05 00:27:39','2016-06-05 00:27:39'),(30,133,'talla 38',1,'','2016-06-05 00:33:32','2016-06-05 00:33:32'),(31,133,'talla 39',1,'','2016-06-05 00:37:13','2016-06-05 00:37:13'),(32,126,'talla 37',1,'','2016-06-05 00:38:09','2016-06-05 00:38:09'),(33,126,'talla 38',1,'','2016-06-05 00:38:34','2016-06-05 00:38:34'),(35,122,'talla 36',1,'','2016-06-05 00:43:06','2016-06-05 00:43:42'),(36,122,'talla 37',1,'','2016-06-05 00:47:00','2016-06-05 00:47:00'),(37,122,'talla 38',1,'','2016-06-05 00:47:36','2016-06-05 00:47:36'),(38,122,'talla 39',1,'','2016-06-05 00:48:07','2016-06-05 00:48:07'),(39,119,'talla 37',1,'','2016-06-05 00:48:41','2016-06-05 00:48:41'),(40,119,'talla 38',1,'','2016-06-05 00:49:18','2016-06-05 00:49:18'),(41,161,'talla 38',1,'','2016-06-09 16:02:15','2016-09-03 20:23:12'),(42,173,'37',2,'','2016-09-07 02:38:37','2016-09-07 19:09:19'),(43,179,'37',3,'','2016-10-14 00:27:41','2016-10-14 00:27:41'),(44,178,'37',3,'','2016-10-14 00:28:26','2016-10-14 00:28:26'),(45,177,'37',3,'','2016-10-14 00:29:49','2016-10-14 00:29:49'),(46,175,'37',2,'','2016-10-14 00:30:59','2016-10-14 00:30:59'),(47,175,'38',2,'','2016-10-14 00:31:30','2016-10-14 00:31:30'),(48,175,'39',2,'','2016-10-14 00:32:02','2016-10-14 00:32:02'),(49,174,'Xs',5,'','2016-10-14 00:33:16','2016-10-14 00:33:16'),(50,174,'S',5,'','2016-10-14 00:33:51','2016-10-14 00:33:51'),(51,174,'M',5,'','2016-10-14 00:34:22','2016-10-14 00:34:22'),(52,172,'37',2,'','2016-10-14 00:35:15','2016-10-14 00:35:15'),(53,177,'38',2,'','2016-10-14 00:36:23','2016-10-14 00:36:23'),(54,177,'39',2,'','2016-10-14 00:37:04','2016-10-14 00:37:04'),(55,176,'Negro',5,'','2016-10-14 00:38:29','2016-10-14 00:38:29'),(56,176,'Caramelo',5,'','2016-10-14 00:39:04','2016-10-14 00:39:04'),(57,180,'35',2,'','2016-10-14 00:44:39','2016-10-14 00:44:39'),(58,180,'36',2,'','2016-10-14 00:45:10','2016-10-14 00:45:10'),(59,180,'37',2,'','2016-10-14 00:46:31','2016-10-14 00:46:31'),(60,180,'38',2,'','2016-10-14 00:47:40','2016-10-14 00:47:40'),(61,180,'39',2,'','2016-10-14 00:48:27','2016-10-14 00:48:27');
/*!40000 ALTER TABLE `subproducts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('user','admin') COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email_token` varchar(999) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Alvaro','Tapia','multinauta@gmail.com','multinauta','$2y$10$TfiRl2eC1BCgLCkkpq2cjOvGAj7sq0DVvTfzIeY5HJMBEfgUPrqyy','admin',1,'sanpablo 2002 santiago','eZKq2mEGAc1rAhGVVxw0HtR5QnGIbITfpC01C9wPRvQYtll8nwxiVFyixtIg','2016-02-22 19:17:23','2016-09-26 21:32:08',NULL,NULL),(2,'Adela','Torres','adela@correo.com','adela','$2y$10$s8ZPJNaxcbnhwNCAW.OtH.yt.EIUHKPJCqOQlY37b.7r19rvEDo3O','user',1,'Tonala 321, Jalisco','CYJ9WFm9nm2EPFJG2ZfsqFz8WQlIiFRjZQOVNPPfvItwjGhWre3dEVpzSVGY','2016-02-22 19:17:23','2016-02-25 15:17:55',NULL,NULL),(3,'juan','','juan@juan.juan','','$2y$10$eRCiJtxTXL84UhOcnHo/3.UMxWTLFpdH1CKeLk8fNas0a1TVEApty','user',0,'','uH376jhjpB4Z6kdYD1UfkAKtMTTCiDWVBaM1xS9GCMm9al6vv8vPwawIDiAi','2016-02-22 20:27:32','2016-02-22 20:41:35',NULL,NULL),(14,'Edson','','edsonpc@gmail.com','','$2y$10$pMhk8Sh73cczHDihXowY8u1tw/217peNH7983pj1/zmEuUjHwh15W','admin',0,'','2JRDrlcRXalNBJEhrwuJSWio3LSvuRwmgIVkNCaGnIfAFEc4ArPKO4fVawrI','2016-04-05 16:35:43','2016-09-07 02:46:44',NULL,NULL),(15,'denisse ','','denisseppf@gmail.com','','$2y$10$NIGM.CXZZeBo8MSzPp/lLOfCPN6BR/uenP25m3wRzP8vuV8LUKOtK','user',0,'','rSuSn150cWvLJFkjZAYfCAhouIaTeJZeYLbRdFHxmA1Iiiyd8YUJRJ7359KN','2016-04-05 23:02:13','2016-04-05 23:07:59',NULL,NULL),(16,'Leila','','leilallanosarancibia@gmail.com','','$2y$10$GpA4tuoDvawF7hBYnPUVP.RyGPEUbxkWzz0U9rvlu//qiwYzW5LGu','user',0,'',NULL,'2016-05-14 20:34:30','2016-05-14 20:34:30',NULL,NULL),(17,'Javiera','','javieracalderon28@gmail.com','','$2y$10$LTR4hPqSgqZ4.iv.UCAdce4C3.aKy2HK2OQUU4aNozOpaCAsakJii','user',0,'',NULL,'2016-06-03 03:22:58','2016-06-03 03:22:58',NULL,NULL),(18,'Sofia','','sarceoportus@gmail.com','','$2y$10$vtMsuK2UVayUKhS75liKEenhzLnUmkHmpJXb0dODnqU14z5OYig0C','user',0,'',NULL,'2016-06-04 01:18:42','2016-06-04 01:18:42',NULL,NULL),(19,'maria jose torres alvarez','torres alvarez','m.josetorresalvarez@gmail.com','maria jose','$2y$10$/JkAd.ed/Z7Auzptt6w0DO4CaoFtkMCluCKkv6m7j0mon6syViJbi','user',0,'god bless you 204',NULL,'2016-06-25 23:09:32','2016-06-25 23:09:32',NULL,'84120574'),(20,'Alicia','Brito','aluciabrito17@gmail.com','Alicuty','$2y$10$lHwAs9hupWZhDSY7mFQfAujWW9gXuZ0lYa4jC7RfQe06NGTADja.e','user',0,'Rodriguez 710',NULL,'2016-07-04 16:13:45','2016-07-04 16:13:45',NULL,''),(21,'Darinka ','Vargas Leyton','darinka.vargas@gmail.com','Darinka','$2y$10$pmr9IhgwZg28pBmu6mpVPOgJQin/JScP8vQmpiz3D/ahlMJqpreb2','user',0,'Ecuador #1150 villa alemana','SaS62lGLJvMLpZFdFOyfK7hnSArC9XF397xkIdravI5kp2hbFPI9ANMoLnHW','2016-07-13 21:17:34','2016-09-28 14:58:30',NULL,'+56979579921'),(22,'Giovanna ','Villalon Pascenti','gvillalonp@gmail.com','giovi.vp','$2y$10$c0IA6Qlj786sFFpr6oxKR.fTThYAJPje3C/1znRgDLxOCGhRA6i7q','user',0,'Amapolas 1500 depto 304 Providencia',NULL,'2016-08-18 18:25:54','2016-08-18 18:25:54',NULL,'+56997419390'),(23,'soledad','rubio','soledadrubiog@gmail.com','soledad','$2y$10$HNIEBX5WaLrytYARs0QBCOGbLYUAeIloqy7aXvzKNGKpCDTzn/Sai','user',0,'peñablanca 372',NULL,'2016-08-30 18:22:35','2016-08-30 18:22:35',NULL,'93268887');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-15 17:31:23
