<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubOrder extends Model
{
    //
	 protected $table = 'sub_orders';
	protected $fillable = ['order_item_id', 'detail', 'stock', 'comment'];
    //
	
	
	
	    // Relation with Product
    public function product()
    {
        return $this->belongsTo('App\OrderItem');
    }
}
