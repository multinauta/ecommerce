<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subproduct extends Model
{

 protected $table = 'subproducts';
	protected $fillable = ['product_id', 'name', 'stock', 'comment'];
    //
	
	
	
	    // Relation with Product
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
