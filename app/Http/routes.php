<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::bind('product', function($slug){
	return App\Product::where('slug', $slug)->first();
});
// Category dependency injection
Route::bind('category', function($category){
    return App\Category::find($category);
});
// Subproduct dependency injection
Route::bind('subproduct', function($subproduct){
    return App\Subproduct::find($subproduct);
});
// User dependency injection
Route::bind('user', function($user){
    return App\User::find($user);
});
Route::bind('user2', function($user){
    return App\User::find($user);
});
Route::bind('user3', function($user){
    return App\User::find($user);
});

Route::get('/', [
	'as' => 'home',
	'uses' => 'StoreController@index'
]);
Route::get('product/{slug}', [
	'as' => 'product-detail',
	'uses' => 'StoreController@show'
]);
// Carrito -------------
Route::get('cart/show', [
	'as' => 'cart-show',
	'uses' => 'CartController@show'
]);

Route::post('cart/add/{product}', [
	'as' => 'cart-add',
	'uses' => 'CartController@add'
]);


Route::get('cart/delete/{product}',[
	'as' => 'cart-delete',
	'uses' => 'CartController@delete'
]);
Route::get('cart/trash', [
	'as' => 'cart-trash',
	'uses' => 'CartController@trash'
]);
Route::get('cart/update/{product}/{quantity}', [
	'as' => 'cart-update',
	'uses' => 'CartController@update'
]);
Route::get('order-detail', [
	'middleware' => 'auth:user',
	'as' => 'order-detail',
	'uses' => 'CartController@orderDetail'
]);
// Authentication routes...
Route::get('auth/login', [
	'as' => 'login-get',
	'uses' => 'Auth\AuthController@getLogin'
]);
Route::post('auth/login', [
	'as' => 'login-post',
	'uses' => 'Auth\AuthController@postLogin'
]);
Route::get('auth/logout', [
	'as' => 'logout',
	'uses' => 'Auth\AuthController@getLogout'
]);
// Registration routes...
Route::get('auth/register', [
	'as' => 'register-get',
	'uses' => 'Auth\AuthController@getRegister'
]);
Route::post('auth/register', [
	'as' => 'register-post',
	'uses' => 'Auth\AuthController@postRegister'
]);

//recover

Route::get('auth/recover', [
	'as' => 'recover-get',
	'uses' => 'Auth\RecoverController@index'
]);

Route::post('auth/recover', [
	'as' => 'recover-post',
	'uses' => 'Auth\RecoverController@recover'
]);

Route::get('auth/modify/{token}', [
	'as' => 'modify-get',
	'uses' => 'Auth\ModifyController@index'
]);

Route::post('auth/modify', [
	'as' => 'modify-post',
	'uses' => 'Auth\ModifyController@modify'
]);

//Flow
Route::post('flow/confirmacion', [

	'as' => 'flow-confirmacion',
	'uses' => 'FlowController@confirmacion'
]);

Route::post('flow/fracaso', [

	'as' => 'flow-fracaso',
	'uses' => 'FlowController@fracaso'
]);

Route::post('flow/exito', [
	
	'as' => 'flow-exito',
	'uses' => 'FlowController@exito'
]);

//Khipu
Route::post('khipu/confirmacion', [

	'as' => 'khipu-confirmacion',
	'uses' => 'KhipuController@confirmacion'
]);

Route::get('khipu/fracaso', [

	'as' => 'khipu-fracaso',
	'uses' => 'KhipuController@fracaso'
]);

Route::get('khipu/exito', [

	'as' => 'khipu-exito',
	'uses' => 'KhipuController@exito'
]);

// ADMIN -------------
Route::group(['namespace' => 'Admin', 'middleware' => ['auth'], 'prefix' => 'admin'], function()
{

	Route::get('home', function(){
		return view('admin.home');
	});

	Route::resource('category', 'CategoryController');
	Route::resource('product', 'ProductController');
	Route::resource('subproduct', 'SubproductController');
	Route::resource('user', 'UserController');
	Route::resource('stock', 'StockController');
	Route::resource('reporteventas', 'ReporteVentasController');
	
	
	Route::get('orders', [
		'as' => 'admin.order.index',
		'uses' => 'OrderController@index'
	]);
	Route::post('order/get-items', [
	    'as' => 'admin.order.getItems',
	    'uses' => 'OrderController@getItems'
	]);

	
	Route::get('order/{id}', [
	    'as' => 'admin.order.destroy',
	    'uses' => 'OrderController@destroy'
	]);
	
	
		Route::post('stock', [
	    'as' => 'admin.stock.index',
	    'uses' => 'StockController@index'
	]);
	
	
	
//	Route::get('/getexport','StockController@getexport');
	
	Route::get('getexport/{cbcstock}', [
	    'as' => 'admin.stock.getexport',
	    'uses' => 'StockController@getexport'
	]);	
	
	
	
		Route::post('getexportventa/{desde}', [
	    'as' => 'admin.reporteventas.getexport',
	    'uses' => 'ReporteVentasController@getexport'
	]);	
	
	
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function()
{

		Route::resource('user2', 'UserController@edit2');

});
	
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function()
{

		Route::resource('user3', 'UserController@update');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function()
{

		Route::get('mispedidos', [
	    'as' => 'admin.user4.mispedidos',
	    'uses' => 'UserController@mispedidos'
	]);	

});
