<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SaveProductRequest;
use App\Http\Controllers\Controller;
use App\Product;
use App\Subproduct;

Use \Input as Input;

class SubproductController extends Controller
{
    public function index(Request $request)
    {
        $subproducts = Subproduct::orderBy('id','desc')->where('product_id','=',$request->get('producto'))->paginate(5);
		$products = Product::orderBy('id','desc')->paginate(95);
		$postproducto = $request->get('producto');

		//dd($products);
		return view('admin.subproduct.index',compact('subproducts','products','postproducto'));
    }
	
	
	    public function store(Request $request)
    {
			$data2 = [
            'product_id'          => $request->get('idproducto'),
            'name'          => $request->get('name'),
            'stock'   => $request->get('stock'),
            'comment'       => $request->get('comment')
        ];
        $subproduct = Subproduct::create($data2);
        $message = $subproduct ? 'Subproducto agregado correctamente!' : 'El subproducto NO pudo agregarse!';
        
        return redirect()->route('admin.subproduct.index')->with('message', $message);
		
		
    }
	
	
	    public function create(Request $request)
    {
        $subproducts = Subproduct::orderBy('id','desc')->lists('name','id');
		$product = Product::orderBy('id','desc')->where('id','=',$request->get('producto'))->paginate(5);
		//dd($categories);
		return view('admin.subproduct.create',compact('subproducts','product'));
    }
	
	
	
	    public function crear(Request $request)
    {
		
	
		//validación
      
    }
	
	    public function destroy(Subproduct $subproduct)
    {
        $deleted = $subproduct->delete();
        
        $message = $deleted ? 'Subproducto eliminado correctamente!' : 'El subproducto NO pudo eliminarse!';
        
        return redirect()->route('admin.subproduct.index')->with('message', $message);
    }
	
	    public function edit(Subproduct $subproduct)
    {
           $products = Product::orderBy('id', 'desc')->lists('name', 'id');
		   return view('admin.subproduct.edit', compact('products', 'subproduct'));
    }
	
	
	
	    public function update(Request $request, Subproduct $subproduct)
    {
          $subproduct->fill($request->all());
        $subproduct->id = $request->get('id');
        
        $updated = $subproduct->save();
        
        $message = $updated ? 'Subproducto actualizado correctamente!' : 'El Subproducto NO pudo actualizarse!';
        
        return redirect()->route('admin.subproduct.index')->with('message', $message);
   
 
    }
	
	
}
