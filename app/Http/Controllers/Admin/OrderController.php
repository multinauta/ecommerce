<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Product;
use DB;

class OrderController extends Controller
{
    public function index()
	{
		$orders = Order::orderBy('id', 'desc')->where('status_id','=','1')->paginate(5);
		//dd($orders);
		return view('admin.order.index',compact('orders'));
	}
	
    public function getItems(Request $request)
    {
	
	 //   $items = OrderItem::with('product')->where('order_id', $request->get('order_id'))->get();
		
    	$items = OrderItem::with('product')->where('order_id', $request->get('order_id'))
				 ->join('sub_orders','order_items.id','=','sub_orders.order_item_id')
		         ->get();
		
	//	$items = DB::table('products')
	//					->select('*')
	//					->join('order_items','order_items.product_id','=','products.id')
	//					->join('sub_orders','order_items.id','=','sub_orders.order_item_id')
	//					->where('order_items.order_id','=',$request->get('order_id'))->paginate(6);
			

		
		file_put_contents('/tmp/qwer.txt',print_r($items,true) );
    	return json_encode($items);
		//dd($request);
    }
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        
        $deleted = $order->delete();
        
        $message = $deleted ? 'Pedido eliminado correctamente!' : 'El Pedido NO pudo eliminarse!';
        
        return redirect()->route('admin.order.index')->with('message', $message);
    }
}
