<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\SubOrder;
use DB;
use Excel;

Use \Input as Input;

class ReporteVentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.reporteventas.index');
    }
    public function getexport(Request $request)
    {
	
	
	
$d=$request->get('desde');
$h=$request->get('hasta');
$cadena=$d.'-'.$h;
$arreglo=explode("-",$cadena);
						
						
	Excel::create('Reporte de Ventas-'.date("d-m-Y h:i:s A"), function($excel) use($arreglo){

    $excel->sheet('Excel sheet', function($sheet)  use($arreglo) {
	
	
	/// creamos los arreglos
	$fecha_inicio = $arreglo[0];
	$fecha_final = $arreglo[1];
	
	$fecha_inicio = $fecha_inicio.' 00:00:00';
	$fecha_final = $fecha_final.' 23:59:59';
	
	$elementos = DB::table('orders')
						->select('orders.id as oid','orders.subtotal as osubtotal','orders.created_at as ofecha','sub_orders.detail as sdetail','sub_orders.stock as stock','users.name as name','users.email as email')
						->join('sub_orders','orders.id','=','sub_orders.order_item_id')
						->join('users','users.id','=','orders.user_id')
						->whereBetween('orders.created_at', array($fecha_inicio ,$fecha_final))
					//	->whereBetween('orders.created_at', array('2016-06-06 00:00:00' ,'2016-06-13 23:59:59'))
						->paginate(10000);	
	
	
	// fin creacion arreglos
	
	$sheet->setStyle(array(
    'font' => array(
        'name'      =>  'Calibri',
        'size'      =>  14,
    )
));
	             $sheet->rows(array(
			 array('ID','SUBTOTAL','FECHA','DETALLE','STOCK','CLIENTE','CORREO CLIENTE')
               )
			   );
	
		foreach($elementos as $elemento)
		{	 
	
	         
                 $sheet->rows(array(		
			                  array($elemento->oid,$elemento->osubtotal,$elemento->ofecha,$elemento->sdetail,$elemento->stock,$elemento->name,$elemento->email)
									)
							 );
		}	

    });

	
	
})->export('xlsx');						
						
						
						
						
						
						
						
						
						
						
	
	//dd($elementos);
    //    return view('admin.reporteventas.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
