<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Subproduct;
use DB;
use Excel;



Use \Input as Input;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
	//	$elementos = Subproduct::orderBy('id','desc')->where('product_id','=',$request->get('producto'))->paginate(5);
	
	 $control = $request->get('cbcstock');
	 
	   if($control=='0' or $control==null)
	   {
		$elementos = DB::table('products')
						->select('products.id as id','products.name as productname','products.price as price'
								,'subproducts.name as subproductname','subproducts.stock as stock')
						->leftjoin('subproducts','products.id','=','subproducts.product_id')->paginate(6);
		}
		else
		{
		$elementos = DB::table('products')
						->select('products.id as id','products.name as productname','products.price as price'
								,'subproducts.name as subproductname','subproducts.stock as stock')
						->join('subproducts','products.id','=','subproducts.product_id')->paginate(6);		
		
		}
		
		
		
		
		
		
		
		return view('admin.stock.index',compact('elementos','control'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

		
    }

	
	    public function getexport(Request $request)
    {

	 $control = $request->get('cbcstock');
	 
	
			
	Excel::create('Stock-'.date("d-m-Y h:i:s A"), function($excel) use($control){

    $excel->sheet('Excel sheet', function($sheet)  use($control) {
	
	
	/// creamos los arreglos
	
	
	
	
	 	   if($control=='0' or $control==null)
	   {
		$elementos = DB::table('products')
						->select('products.id as id','products.name as productname','products.price as price'
								,'subproducts.name as subproductname','subproducts.stock as stock')
						->leftjoin('subproducts','products.id','=','subproducts.product_id')->paginate(10000);
		}
		else
		{
		$elementos = DB::table('products')
						->select('products.id as id','products.name as productname','products.price as price'
								,'subproducts.name as subproductname','subproducts.stock as stock')
						->join('subproducts','products.id','=','subproducts.product_id')->paginate(10000);		
		
		}
	
	// fin creacion arreglos
	
	$sheet->setStyle(array(
    'font' => array(
        'name'      =>  'Calibri',
        'size'      =>  14,
    )
));
	             $sheet->rows(array(
			 array('ID','NOMBRE PRODUCTO','PRECIO','NOMBRE SUBPRODUCTO','STOCK')
               )
			   );
	
		foreach($elementos as $elemento)
		{	 
			if($elemento->subproductname==null)$subname='-';else $subname=$elemento->subproductname;
	          if($elemento->stock==null)$stock='0';else $stock=$elemento->stock;
                 $sheet->rows(array(		
			                  array($elemento->id,$elemento->productname,$elemento->price,$subname,$stock)
									)
							 );
		}	

    });

	
	
})->export('xlsx');
	
    }
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
