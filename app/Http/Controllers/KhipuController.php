<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Khipu;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\SubOrder;
use App\SubProduct;
use DB;
use Auth;

class KhipuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmacion()
    {

        $data = array(

            'notification_token'  => $_POST['notification_token'],
        );

     
        $notification_payments = khipu::loadService('GetPaymentNotification');

        foreach ($data as $name => $value):

            $notification_payments->setParameter($name, $value);
        
        endforeach;

        $response = $notification_payments->consult();
        $arreglo=json_decode($response);
        $ORDEN_NUMERO = $arreglo->transaction_id;

        $order = Order::where('order_id', $ORDEN_NUMERO )->first();
        $order->status_id = 1;
        $order->save();

        $this->actualizarStock($ORDEN_NUMERO);

    }

    public function actualizarStock($numeroOrden){


    	$order_id = Order::where('order_id', $numeroOrden)->first();

	 	$productosOrden = OrderItem::where('order_id', $order_id->id)->lists('id');
	 	

		foreach($productosOrden as $idItem)
		{

			$idProducto = DB::table('order_items')->where('id', $idItem)->value('product_id');
			$quantity = DB::table('order_items')->where('id', $idItem)->value('quantity');
            
			$stock = DB::table('subproducts')->where('product_id', $idProducto)->value('stock');
            $detail = DB::table('subproducts')->where('product_id', $idProducto)->value('name');


             SubOrder::create([
            'order_item_id' => $idItem,
            'detail' => $detail,
            'stock' => number_format($quantity)
            ]);


			$new_stock = number_format($stock) - number_format($quantity);

			DB::table('subproducts')
            ->where('product_id', $idProducto)
            ->update(['stock' => $new_stock]);

            
		}
    }


    public function exito()
	{

        \Session::forget('cart');
	    return \Redirect::route('home')->with('message', 'Compra realizada de forma correcta');
	}

	public function fracaso()
	{
	   
	    return \Redirect::route('home')->with('message', 'Hubo un problema al intentar pagar con Webpay');
	}
    
}
