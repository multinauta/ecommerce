<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Subproduct;
use DB;
Use \Input as Input;

class StoreController extends Controller
{
    public function index(Request $request)
	{
		$busqueda = $request->input('busqueda');
		$idcategoria = $request->input('categoria');
		//$busqueda = Input::get('busqueda');
		//$products = Product::all();
		//$products = Product::orderBy('id','desc')->paginate(8);
		
		if($idcategoria=='' or $idcategoria=='todas')
		{
	//	$products = Product::where('name','LIKE','%'.$busqueda.'%')->orderBy('id','desc')->paginate(8);
		
						$products = DB::table('products')
						->select('products.id as id','products.name as name','products.slug as slug'
						         ,'products.description as description','products.extract as extract','products.price as price'
								 ,'products.image as image','products.visible as visible','products.stock as stock')
						->leftjoin('subproducts','products.id','=','subproducts.product_id')
						->where('products.name','LIKE','%'.$busqueda.'%')
						->orWhere('subproducts.name','LIKE','%'.$busqueda.'%')
						->groupBy('products.id')
						->orderBy('id','desc')->paginate(8);  
		}
		else
		{
		
		$products = Product::where('category_id',$idcategoria)->orderBy('id','desc')->paginate(8);
		}
	
		//dd($busqueda);
		$categorias = Category::all();
		return view('store.index',compact('products','categorias'));
	}
	
	public function show($slug)
	{
		
		$product = Product::where('slug',$slug)->first();
	//	$subproducts = Subproduct::where('product_id','=',$product->id);
		$subproducts = Subproduct::orderBy('id','desc')->where('product_id','=',$product->id)->paginate(100);
	//	dd($subproducts);
		return view('store.show',compact('product','subproducts'));
		
	}
	
	
}
