<?php

namespace App\Http\Controllers;

use Flow;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Order;
use App\OrderItem;
use App\SubOrder;
use App\SubProduct;
use DB;
use Auth;
use DateTime;

Use \Input as Input;

class CartController extends Controller
{
	
	public function __construct()
	{
		if(!\Session::has('cart')) \Session::put('cart',array());
		
	}
    //show cart
	
	public function show(Request $request)
	{
		
	  //  $cantidad = $request->get('cantidad');
		$cart =  \Session::get('cart');
		$total = $this->total();
	
	  
		return view('store.cart',compact('cart', 'total'));
	//	dd($cantidad);
		
		
	}
	
	//add item
	
	public function add(Request $request,Product $product)
	
	{
	
	   $listado = array();
	   $valor=number_format($request->get('largoarreglo'));
	   for($i=0;$i <= $valor;$i++)
	   {
	   $nombre='idsubproducto'.$i;
	   $nombre1='namesubproducto'.$i;
	   $nombre2='cantidad'.$i;
	   if ($request->get($nombre2)!=0)
	   {
	   $listado[$i]=$request->get($nombre).'-'.$request->get($nombre1).'-'.$request->get($nombre2);
	   }
	   }
	
		$cart = \Session::get('cart');
		$product3=Product::where('slug', $request->get('slug'))->first();
		$product3->quantity = number_format($request->get('totalcantidad'));
		$product3->detalle=$listado;
		$cart[$product3->slug]= $product3;
		
		\Session::put('cart',$cart);
	
		// dd($product3);
		return redirect()->route('cart-show');
/*	
	$enviar= array();
	$subproductos = array();
	$i=0;
	foreach($request->all() as $arreglo)
	
	{
	   

	 $enviar[$i]=$arreglo;
	 $i++;
	 
	}
		
		dd($request->all());
		*/
	}
	//delete item
	public function delete(Product $product)
	{
		
		$cart = \Session::get('cart');
		unset($cart[$product->slug]);
		\Session::put('cart',$cart);
		return redirect()->route('cart-show');
		
		
	}
	
	
	//update item
	public function update(Product $product,$quantity)
	{
		$cart = \Session::get('cart');
		$cart[$product->slug]->quantity = $quantity;
		\Session::put('cart',$cart);
		
		return redirect()->route('cart-show');
		

	}	
	//trash cart
	
	public function trash()
	{
		\Session::forget('cart');
		
		return redirect()->route('cart-show');
	}	
	
	//total
    public function total()
	{
			$cart = \Session::get('cart');
			$total = 0;
			foreach($cart as $item){
				$total += $item->price * $item->quantity;
			}
		
		return $total;
	}
	
	 public function orderDetail(Request $request)
	 {

		 if(count(\Session::get('cart')) <= 0) return redirect()->route('home');
		 $cart = \Session::get('cart');
		 
		 
		 if($request->get('cambiar')=="true")
		 {
		 $direccion= $request->get('direccion');
		 }
		 else
		 {
		 $direccion= Auth::user()->address;
		 }
		 
		
		 
		 \Session::put('direccion',$direccion);
	//	 \Session::put('cart',$cart);
		 $total = $this->total();
		 
		 $fechaHoraSegundos = new DateTime();
		 $orden_compra =  Auth::user()->id.$fechaHoraSegundos->format('YmdHis');


        $orden = [

            'orden_compra'  =>  $orden_compra,
            'monto'         => $total,
            'concepto'      => 'Pago de Orden N° ' . $orden_compra,
            'email_pagador' => Auth::user()->email,

            // Opcional: Medio de Pago (Webpay = 1, Servipag = 2, Ambos = 9)
            //'medio_pago'    => $request->input('medio_pago'),
        ];

        // Genera una nueva Orden de Pago, Flow la firma y retorna un paquete de datos firmados
        $orden['flow_pack'] = Flow::new_order($orden['orden_compra'], $orden['monto'], $orden['concepto'], $orden['email_pagador']);
	//	dd($request->all());
 
        $this->saveOrder($orden_compra);
        //\Session::forget('cart');

        return view('store.order-detail', compact('cart','total','orden_compra'),$orden);
		 
	 }

	 public function saveOrder($orden_compra)
    {
	
	    $direccion = \Session::get('direccion');
        $subtotal = 0;
        $cart = \Session::get('cart');
        $shipping = 100;

        foreach ($cart as $producto) {
            $subtotal += $producto->quantity * $producto->price;
        }

        $order = Order::create([
            'subtotal' => $subtotal,
            'shipping' => $shipping,
            'user_id' =>  \Auth::user()->id,
			'address' => $direccion,
			'order_id' => $orden_compra
        ]);


		\Session::forget('direccion');

        foreach ($cart as $producto) {
            $this->saveOrderItem($producto, $order->id);
        }
    }

    public function saveOrderItem($producto, $order_id)
    {
	
	   file_put_contents('/tmp/producto.txt',print_r($producto,true));
	
        OrderItem::create([
            'price' => $producto->price,
            'quantity' => $producto->quantity,
            'product_id' => $producto->id,
            'order_id' => $order_id
        ]);
		
			
		
    }

}
