<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use DB;

Use \Input as Input;

class ModifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
	
	//     $token=$request->cookies->get('XSRF-TOKEN');

	
function sacar($TheStr, $sLeft, $sRight){
        $pleft = strpos($TheStr, $sLeft, 0);
        if ($pleft !== false){
                $pright = strpos($TheStr, $sRight, $pleft + strlen($sLeft));
                If ($pright !== false) {
                        return (substr($TheStr, $pleft + strlen($sLeft), ($pright - ($pleft + strlen($sLeft)))));
                }
        }
        return '';
}

	$texto = $request->path()."?";
$token = sacar($texto,"modify/","?");


 $contador=0;
	$validador = DB::table('users')
						->select('*')
						->where('email_token','=',$token)
						->get();
						
						foreach($validador as $item)
						{
						 $contador++;
						}
	if($contador==0)
	{
	return \Redirect::route('recover-get')->with('message', 'El link ya no es valido, debe generar otra solicitud');
	}
        return view('auth.modify',compact('token'));
	 

	   
    }
	
	
	
	public function modify(Request $request)
    {
	
	
    	function sacar($TheStr, $sLeft, $sRight){
            $pleft = strpos($TheStr, $sLeft, 0);
            if ($pleft !== false){
                    $pright = strpos($TheStr, $sRight, $pleft + strlen($sLeft));
                    If ($pright !== false) {
                            return (substr($TheStr, $pleft + strlen($sLeft), ($pright - ($pleft + strlen($sLeft)))));
                    }
            }
            return '';
        }
	   
        $tok= sacar($request->get('token'),"{","}");

	   	DB::table('users')
            ->where('email_token', $tok)
            ->update(['password' => bcrypt($request->get('pass1'))]);
	
			
				   	DB::table('users')
            ->where('email_token', $tok)
            ->update(['email_token' => null]);
			
			
			return \Redirect::route('login-get')->with('message', 'Su contrasena ha sido actualizada');
		
		//dd($request->get('token').'-'.$request->get('pass1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
