<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Mail;
use Session;
use Redirect;
use App\User;
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

Use \Input as Input;

class RecoverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.recover');
    }

	public function recover(Request $request)
    {
	
    $contador=0;
	$validador = DB::table('users')
						->select('*')
						->where('email','=',$request->get('email'))
						->get();
						
						foreach($validador as $item)
						{
						 $contador++;
						}
			
	if($contador!=0)
	{
	$token = md5(uniqid(microtime(), true));
	DB::table('users')
            ->where('email', $request->get('email'))
            ->update(['email_token' => $token]);
			
			$url= url('/auth/modify/'.$token);
		//	$request->url=$url;
			 $request->request->add(['url' => $url]);
			
	Mail::send('auth.mail', $request->all(), function ($message) use($request) {
    $message->from('recovery-password@ssdd.cl', 'Mis Compras');
    $message->to($request->email)->cc('recovery-password@ssdd.cl');
	$message->subject("Cambio de contrasena");
});
	
       return \Redirect::route('recover-get')->with('message', 'Se ha enviado un correo a su casilla - Si no lo encuentra en bandeja de entrada verifique en spam');
			
	}
	else
	{
	return \Redirect::route('recover-get')->with('message', 'Correo no existe');
	}
	


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
