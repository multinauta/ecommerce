<?php

namespace App\Http\Controllers;

use Flow;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\SubOrder;
use App\SubProduct;
use DB;
use Auth;

class FlowController extends Controller
{

    public function confirmacion()
	{
	    try 
	    {
	        // Lee los datos enviados por Flow
	        Flow::read_confirm();

	    }
	    catch (Exception $e) 
	    {
	        // Si hay un error responde false
	        echo Flow::build_response(false);
	        return;
	    }

	    // Recupera los valores de la Orden
	    $FLOW_STATUS  = Flow::getStatus();      // El resultado de la transacción (EXITO o FRACASO)
	    $ORDEN_NUMERO = Flow::getOrderNumber(); // N° Orden del Comercio
	    //$MONTO        = Flow::getAmount();      // Monto de la transacción
	    //$ORDEN_FLOW   = Flow::getFlowNumber();  // Si $FLOW_STATUS = "EXITO" el N° de Orden de Flow
	    //$PAGADOR      = Flow::getPayer();       // El email del pagador


	    /**
	     * Aquí puede validar la Orden
	     * Si acepta la Orden responder Flow::build_response(true)
	     * Si rechaza la Orden responder Flow::build_response(false)
	     */

	    if ($FLOW_STATUS == "EXITO") {
	        // La transacción fue aceptada por Flow
	        // Aquí puede actualizar su información con los datos recibidos por Flow
	        //echo Flow::build_response(true); // Comercio acepta la transacción
			//file_put_contents('/tmp/pruebafloow14.txt',$ORDEN_NUMERO);
	        $order = Order::where('order_id', $ORDEN_NUMERO )->first();
	        $order->status_id = 1;
	        $order->save();
	        \Session::forget('cart');

	        $this->actualizarStock($ORDEN_NUMERO);

	    } else {

	        echo Flow::build_response(false); // Comercio rechaza la transacción
	   	}
	   
	}

	 public function actualizarStock($numeroOrden){


    	$order_id = Order::where('order_id', $numeroOrden)->first();

	 	$productosOrden = OrderItem::where('order_id', $order_id->id)->lists('id');
	 	

		foreach($productosOrden as $idItem)
		{

			$idProducto = DB::table('order_items')->where('id', $idItem)->value('product_id');
			$quantity = DB::table('order_items')->where('id', $idItem)->value('quantity');

			$stock = DB::table('subproducts')->where('product_id', $idProducto)->value('stock');
			$detail = DB::table('subproducts')->where('product_id', $idProducto)->value('name');

             SubOrder::create([
            'order_item_id' => $idItem,
            'detail' => $detail,
            'stock' => number_format($quantity)
            ]);

			$new_stock = number_format($stock) - number_format($quantity);

			DB::table('subproducts')
            ->where('product_id', $idProducto)
            ->update(['stock' => $new_stock]);
	
		}
    }


	public function exito()
	{

        \Session::forget('cart');
	    return \Redirect::route('home')->with('message', 'Compra realizada de forma correcta');
	}

	public function fracaso()
	{
	   
	    return \Redirect::route('home')->with('message', 'Hubo un problema al intentar pagar con Webpay');
	}
}
