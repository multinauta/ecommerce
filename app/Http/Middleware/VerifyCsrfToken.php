<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [

        'flow/confirmacion',
        'flow/exito',
        'flow/fracaso',
        'khipu/confirmacion',
        'khipu/exito',
        'khipu/fracaso',

    ];
}
