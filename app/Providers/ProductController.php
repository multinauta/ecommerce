<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SaveProductRequest;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;

Use \Input as Input;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
        $products = Product::orderBy('id','desc')->paginate(5);
		//dd($products);
		return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
				
        $categories = Category::orderBy('id','desc')->lists('name','id');
		//dd($categories);
		return view('admin.product.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveProductRequest $request)
    {
	
		// creamos un token para nombrar las imagenes y que no se repitan
	      $nombreToken=   str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());
          $nombre = str_slug($request->get('name')).$nombreToken.".jpg";
		  
		
			$file = $request->file('image');
			\Storage::disk('local')->put($nombre,  \File::get($file));
	 

	   
	       $ruta = "img/catalogo/".$nombre;
		
	
		
		//validación
         $data = [
            'name'          => $request->get('name'),
            'slug'          => str_slug($request->get('name')),
            'description'   => $request->get('description'),
            'extract'       => $request->get('extract'),
            'price'         => $request->get('price'),
            'image'         => $ruta,
            'visible'       => $request->has('visible') ? 1 : 0,
            'category_id'   => $request->get('category_id')
        ];
        $product = Product::create($data);
        $message = $product ? 'Producto agregado correctamente!' : 'El producto NO pudo agregarse!';
        
        return redirect()->route('admin.product.index')->with('message', $message);
    }

	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
           $categories = Category::orderBy('id', 'desc')->lists('name', 'id');
		   return view('admin.product.edit', compact('categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveProductRequest $request, Product $product)
    {
		
			
		if($request->file('image2')!=null)
		{
			      $nombreToken=   str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());
          $nombre = str_slug($request->get('name')).$nombreToken.".jpg";
		
		
		
			$file = $request->file('image2');
			\Storage::disk('local')->put($nombre,  \File::get($file));
	
	  $ruta = "img/catalogo/".$nombre;
		        
	    }
		
		
          $product->fill($request->all());
        $product->slug = str_slug($request->get('name'));
        $product->visible = $request->has('visible') ? 1 : 0;
		if($request->file('image2')!=null)
		{
			$product->image = $ruta;	
		}
        $updated = $product->save();
        
        $message = $updated ? 'Producto actualizado correctamente!' : 'El Producto NO pudo actualizarse!';
        
        return redirect()->route('admin.product.index')->with('message', $message); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $deleted = $product->delete();
        
        $message = $deleted ? 'Producto eliminado correctamente!' : 'El producto NO pudo eliminarse!';
        
        return redirect()->route('admin.product.index')->with('message', $message);
    }
}
